# encoding: utf-8

import numpy as np

# User Parameters
tran_power = 0.2  # transmission power   23dBm ≈ 0.2W
ME_CPU = 450  # 150MHz   20ms可以3MCycle  100MHz   20ms可以2MCycle  50 20ms可以1MCycle


# user's data size     0.3 Mbits  指数分布  均值为0.3
def get_data_size():
    # return np.random.exponential(0.3)
    return np.random.poisson(0.3)
    # return np.random.uniform(0.1, 0.5)  # 均匀分布[0.1, 0.5)


# 每bit数据所需的CPU周期数  [4, 15)，即[4, 14]cycle/bit      期望为9
def get_cycle_per_bit():
    return np.random.randint(4, 15)


# Edge Parameters
B = 20 * pow(10, 6)  # users' bandwidth     20Mhz
noise = pow(10, -13)  # -90dBm = pow(10, -13)
# Edge_CPU = ME_CPU * 10  # 1500 MHz
Edge_CPU = 1200

Edge_num = 6  # edge's num
ME_num = 5  # 每个edge有多少User


# Edge之间传输速度
def get_edges_speed():
    return np.random.uniform(1250, 1350)  # 1250-1350Mbits/s
# Edges_speed = np.random.uniform(350, 450)  # Mbits/s


# Cloud Parameters
def get_edge2cloud_speed():
    return np.random.uniform(15, 25)  # Mbits/d
# Edge2Cloud_speed = np.random.uniform(25, 35)  # Mbits/d
