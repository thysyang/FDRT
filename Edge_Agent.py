# encoding: utf-8

import torch
import torch.optim as optim
import torch.nn as nn
import numpy as np
import os
import Config as config
from Net import Edge_Net
from ME_Agent import Queue, ReplayBuffer

# Hyper Parameters
BATCH_SIZE = 32
LR = 0.0003  # learning rate
EPSILON = 0.9  # greedy policy
GAMMA = 0.9  # reward discount
TARGET_REPLACE_ITER = 100  # target update frequency
MEMORY_CAPACITY = 1000

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class Edge_Agent_common:
    def __init__(self, adjacent_matrix, edges_speed, edge2cloud_speed):
        self.adjacent_matrix = adjacent_matrix
        self.edge_task = [Queue() for i in range(config.Edge_num)]  # 每个edge的task个数以及总cycle
        self.edges_speed = edges_speed
        self.edge2cloud_speed = edge2cloud_speed

    def calculate(self):
        for task in self.edge_task:
            task.calculate(config.Edge_CPU)

    # reset 每个edge的队列
    def reset(self):
        for queue in self.edge_task:
            queue.reset()

    def print(self):
        for queue in self.edge_task:
            queue.print()


class Edge_Agent:
    def __init__(self, num, adj_num, edge_setting, edge_type):
        self.num = num
        self.type = edge_type
        self.flag = 0  # 用来显示是否开始learn

        self.state_dim = (adj_num + 1) * 2 + 2  # 自己及相邻edge队列状态 + data_size + cycle

        # 0-local, 1-cloud, other-other's edge
        self.action_dim = adj_num + 2  # action number(与相邻的edge个数相关) + local + cloud

        self.eval_net, self.target_net = \
            Edge_Net(self.state_dim, self.action_dim), Edge_Net(self.state_dim, self.action_dim)

        self.learn_step_counter = 0  # for target updating
        self.memory = ReplayBuffer(self.state_dim, MEMORY_CAPACITY)
        self.optim = optim.Adam(self.eval_net.parameters(), lr=LR)
        self.loss_func = nn.MSELoss()

        self.eval_net.to(device)
        self.target_net.to(device)

        self.setting = edge_setting

        # 当前状态的前一个状态、动作和奖励
        self.s = None
        self.a = None
        self.r = None

        self.loss = None

        self.reward = 0
        self.Reward = []
        self.reward_file = './data/' + edge_type + '_edge_' + str(self.num) + '_reward.csv'

        self.local = 0  # 每个episode的本地计算动作
        self.Local = []
        self.local_file = './data/' + edge_type + '_edge_' + str(self.num) + '_local.csv'

        self.cloud = 0  # 每个episode的cloud动作
        self.Cloud = []
        self.cloud_file = './data/' + edge_type + '_edge_' + str(self.num) + '_cloud.csv'

        self.other = 0  # 每个episode的other edge动作
        self.Other = []
        self.other_file = './data/' + edge_type + '_edge_' + str(self.num) + '_other.csv'

        self.times = []
        self.time = []
        self.times_file = './data/' + edge_type + '_edge_' + str(self.num) + '_times.csv'

        self.local_times = []
        self.local_time = []  # 每个episode的所有本地计算时间
        self.local_times_file = './data/' + edge_type + '_edge_' + str(self.num) + '_local_times.csv'
        self.cloud_times = []
        self.cloud_time = []  # 每个episode的所有cloud时间
        self.cloud_times_file = './data/' + edge_type + '_edge_' + str(self.num) + '_cloud_times.csv'
        self.other_times = []
        self.other_time = []  # 每个episode的所有other时间
        self.other_times_file = './data/' + edge_type + '_edge_' + str(self.num) + '_other_times.csv'

    def transform_state(self, data_size, cycle):
        state = [data_size, cycle, len(self.setting.edge_task[self.num].q), self.setting.edge_task[self.num].cycle]
        adj = self.setting.adjacent_matrix[self.num]
        for i in range(config.Edge_num):
            if adj[i] == 1:
                state.append(len(self.setting.edge_task[i].q))
                state.append(self.setting.edge_task[i].cycle)
        return state

    def store_transition(self, s, a, r, s_):
        self.memory.store_transition(s, a, r, s_)

    # 0-local, 1-cloud, other-other edge
    def choose_action(self, s):
        if np.random.random() < EPSILON:
            s = torch.FloatTensor(s).to(device)
            advantage = self.eval_net.advantage(s)
            action = torch.argmax(advantage).item()
            # print(str(self.num) + '  ' + str(action))
        else:
            action = np.random.choice(self.action_dim)

        return action

    # 0-local, 1-cloud, other-other edge
    def choose_action_(self, s):
        s = torch.FloatTensor(s).to(device)
        advantage = self.eval_net.advantage(s)
        action = torch.argmax(advantage).item()
        return action

    def learn(self):
        if self.memory.mem_cntr < self.memory.mem_size:
            return

        if self.flag == 0:
            print('--------------' + self.type + '------------------edge'
                  + str(self.num) + ' start learning---------------------------------')
            self.flag = 1

        if self.learn_step_counter % TARGET_REPLACE_ITER == 0:
            self.target_net.load_state_dict(self.eval_net.state_dict())

        states, actions, rewards, states_ = self.memory.sample(BATCH_SIZE)

        states = torch.tensor(states).to(device)
        rewards = torch.tensor(rewards).to(device)
        actions = torch.tensor(actions).to(device)
        states_ = torch.tensor(states_).to(device)

        indices = np.arange(BATCH_SIZE)

        q_eval = self.eval_net(states)[indices, actions]
        # q_eval = self.eval_net(states).gather(1, actions)
        q_next = self.target_net(states_)

        a_best = torch.argmax(self.eval_net(states_), dim=1)
        # q_eval = self.q_eval(torch.Tensor(states_).to(device))[indices, actions]
        q_target = rewards + GAMMA * q_next[indices, a_best]

        self.optim.zero_grad()

        loss = self.loss_func(q_target, q_eval)
        self.loss = loss
        loss.backward()

        self.optim.step()

        # self.epsilon = self.epsilon - self.epsilon_decay if self.epsilon > self.eps_min else self.eps_min
        self.learn_step_counter += 1

    def train(self, data_size, cycle):
        s = self.transform_state(data_size, cycle)

        if self.s is not None:
            self.store_transition(self.s, self.a, self.r, s)

        a = self.choose_action(s)
        if a == 0:
            self.setting.edge_task[self.num].add(data_size, cycle)
            time = self.setting.edge_task[self.num].cycle / config.Edge_CPU  # 单位: s
            self.local += 1
            self.local_time.append(time)
        elif a == 1:  # 由于cloud计算很快，这里忽略不计，只考虑edge到cloud的传输时间
            # time = data_size / config.Edge2Cloud_speed
            time = data_size / self.setting.edge2cloud_speed[self.num]
            self.cloud += 1
            self.cloud_time.append(time)
        else:
            index = 0
            count = a - 1
            adj = self.setting.adjacent_matrix[self.num]
            for i in range(config.Edge_num):
                if adj[i] == 1:
                    count -= 1
                    index = i
                if count == 0:
                    break
            self.setting.edge_task[index].add(data_size, cycle)
            # time_tran = data_size / config.Edges_speed
            time_tran = data_size / self.setting.edges_speed[self.num][index]
            time_other_cal = self.setting.edge_task[index].cycle / config.Edge_CPU
            time = time_tran + time_other_cal
            self.other += 1
            self.other_time.append(time)

        r = - time

        self.time.append(time)

        self.s = s
        self.a = a
        self.r = r

        # 计算每个episode累积的Reward
        self.reward += r

        self.learn()

        return time

    def run(self, data_size, cycle):
        s = self.transform_state(data_size, cycle)

        a = self.choose_action_(s)
        if a == 0:
            self.setting.edge_task[self.num].add(data_size, cycle)
            time = self.setting.edge_task[self.num].cycle / config.Edge_CPU  # 单位: s
            self.local += 1
            self.local_time.append(time)
        elif a == 1:  # 由于cloud计算很快，这里忽略不计，只考虑edge到cloud的传输时间
            # time = data_size / config.Edge2Cloud_speed
            time = data_size / self.setting.edge2cloud_speed[self.num]
            self.cloud += 1
            self.cloud_time.append(time)
        else:
            index = 0
            count = a - 1
            adj = self.setting.adjacent_matrix[self.num]
            for i in range(config.Edge_num):
                if adj[i] == 1:
                    count -= 1
                    index = i
                if count == 0:
                    break
            self.setting.edge_task[index].add(data_size, cycle)
            # time_tran = data_size / config.Edges_speed
            time_tran = data_size / self.setting.edges_speed[self.num][index]
            time_other_cal = self.setting.edge_task[index].cycle / config.Edge_CPU
            time = time_tran + time_other_cal
            self.other += 1
            self.other_time.append(time)

        self.time.append(time)

        return time

    def print(self):
        # print('-----------Q----------')
        # print(self.eval_net(torch.FloatTensor(self.s).unsqueeze(0).to(device)))
        # print('-----------Q\'----------')
        # print(self.target_net(torch.FloatTensor(self.s).unsqueeze(0).to(device)))
        # print('-----------loss----------')
        print(self.loss)

    def reset(self):
        self.reward = 0
        self.local = 0
        self.cloud = 0
        self.other = 0
        self.time = []
        self.local_time = []
        self.cloud_time = []
        self.other_time = []

    def store(self):
        self.Reward.append(self.reward)
        self.Local.append(self.local)
        self.Cloud.append(self.cloud)
        self.Other.append(self.other)
        self.times.append(np.mean(self.time))
        self.local_times.append(np.mean(self.local_time))
        self.cloud_times.append(np.mean(self.cloud_time))
        self.other_times.append(np.mean(self.other_time))

    def save(self):
        if os.path.exists(self.reward_file):
            os.remove(self.reward_file)
        torch.save(torch.tensor(self.Reward), self.reward_file)

        if os.path.exists(self.local_file):
            os.remove(self.local_file)
        torch.save(torch.tensor(self.Local), self.local_file)

        if os.path.exists(self.cloud_file):
            os.remove(self.cloud_file)
        torch.save(torch.tensor(self.Cloud), self.cloud_file)

        if os.path.exists(self.other_file):
            os.remove(self.other_file)
        torch.save(torch.tensor(self.Other), self.other_file)

        if os.path.exists(self.times_file):
            os.remove(self.times_file)
        torch.save(torch.tensor(self.times), self.times_file)

        if os.path.exists(self.local_times_file):
            os.remove(self.local_times_file)
        torch.save(torch.tensor(self.local_times), self.local_times_file)

        if os.path.exists(self.cloud_times_file):
            os.remove(self.cloud_times_file)
        torch.save(torch.tensor(self.cloud_times), self.cloud_times_file)

        if os.path.exists(self.other_times_file):
            os.remove(self.other_times_file)
        torch.save(torch.tensor(self.other_times), self.other_times_file)
