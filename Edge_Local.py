# encoding: utf-8

import torch
import numpy as np
import os
import Config as config


class Edge_Local:
    def __init__(self, num, edge_setting, edge_type):
        self.num = num
        self.type = edge_type

        self.setting = edge_setting

        self.times = []
        self.time = []
        self.times_file = './data/' + edge_type + '_edge_' + str(self.num) + '_times.csv'

    def train(self, data_size, cycle):
        self.setting.edge_task[self.num].add(data_size, cycle)
        time = self.setting.edge_task[self.num].cycle / config.Edge_CPU  # 单位: s

        self.time.append(time)
        return time

    def reset(self):
        self.time = []

    def store(self):
        self.times.append(np.mean(self.time))

    def save(self):
        if os.path.exists(self.times_file):
            os.remove(self.times_file)
        torch.save(torch.tensor(self.times), self.times_file)
