# encoding: utf-8

import torch
import numpy as np
import os
import Config as config


class Edge_Random:
    def __init__(self, num, adj_num, edge_setting, edge_type):
        self.num = num
        self.type = edge_type

        self.setting = edge_setting
        # 0-local, 1-cloud, other-other's edge
        self.action_dim = adj_num + 2  # action number(与相邻的edge个数相关) + local + cloud

        self.times = []
        self.time = []
        self.times_file = './data/' + edge_type + '_edge_' + str(self.num) + '_times.csv'

    def train(self, data_size, cycle):
        a = np.random.choice(self.action_dim)
        if a == 0:
            self.setting.edge_task[self.num].add(data_size, cycle)
            time = self.setting.edge_task[self.num].cycle / config.Edge_CPU  # 单位: s
        elif a == 1:  # 由于cloud计算很快，这里忽略不计，只考虑edge到cloud的传输时间
            # time = data_size / config.Edge2Cloud_speed
            time = data_size / self.setting.edge2cloud_speed[self.num]
        else:
            index = 0
            count = a - 1
            adj = self.setting.adjacent_matrix[self.num]
            for i in range(config.Edge_num):
                if adj[i] == 1:
                    count -= 1
                    index = i
                if count == 0:
                    break
            self.setting.edge_task[index].add(data_size, cycle)
            time_tran = data_size / self.setting.edges_speed[self.num][index]
            time_other_cal = self.setting.edge_task[index].cycle / config.Edge_CPU
            time = time_tran + time_other_cal

        self.time.append(time)

        return time

    def reset(self):
        self.time = []

    def store(self):
        self.times.append(np.mean(self.time))

    def save(self):
        if os.path.exists(self.times_file):
            os.remove(self.times_file)
        torch.save(torch.tensor(self.times), self.times_file)
