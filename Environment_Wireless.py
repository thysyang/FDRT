# encoding: utf-8

# 这个是用户设备与边的环境模型，主要用来生成信道增益
# 边与用户的无线环境模型是一个马尔科夫链

import numpy as np


class EnvironmentWireless:
    def __init__(self):
        # antenna gain dBi
        self.Ad = 24.5  # 5G AAU形态天线
        # self.Ad = 4.11

        # carrier frequency  Hz   AAU范围：2515~2675MHz
        self.fc = 2595 * pow(10, 6)
        # self.fc = 915 * pow(10, 6)

        # path loss exponent    free-space path loss model
        self.de = 2.8

        # 边与移动设备的实际距离
        # 5G由于频点太高,信号穿透力差,基站的覆盖半径一般约为100-300米
        # 随后随机一个方向（靠近或者远离边）和一个数值（大概是人的移动速度）
        # 初始化为-200-200m [-200, 200)的随机值 负值表示在左侧
        self.d = np.random.uniform(-200, 200)
        # self.d = 200 - np.random.uniform(-200, 200)

        # channel fading factor
        self.fade_factor = np.random.rayleigh(1)  # 使用单位标准偏差的瑞利分布
        # 笔记--各种概率分布的关系

        # UE移动方向   1 right； 0 left
        self.d_direction = np.random.randint(0, 2)

        # UE移动速度 1.5m/s = 1.5mm/ms = 30mm/20ms  (1.48-1.52 = 29.6-30.4)
        self.d_speed = np.random.uniform(0.0296, 0.0305)  # 人步行速度
        # self.d_speed = np.random.uniform(0.36, 0.44)  # 汽车速度

    # 用户（移动设备）移动
    def move(self):
        # 一旦行走方向确定后，用户的移动方向几乎不会改变，有1e-5的概率改变
        self.d_direction = abs(self.d_direction - 1) if np.random.binomial(1, 1e-5) else self.d_direction
        if self.d_direction == 1:
            self.d += self.d_speed
        else:
            self.d -= self.d_speed

        #  用户每个时隙移动速度不同
        self.d_speed = np.random.uniform(0.0296, 0.0305)  # 人步行速度

        # 每个相干时间（20ms）的衰减因子不同
        self.fade_factor = np.random.rayleigh(1)  # 使用单位标准偏差的瑞利分布
        # 笔记--各种概率分布的关系

        # 一旦距离边超过200m说明已经走出当前边的覆盖范围进入临近边的覆盖范围
        # 用户走出当前边覆盖范围后，当前边会将计算结果自动传给临近边，由临近边返回
        if abs(self.d) > 200:
            if self.d_direction == 0:
                self.d = 400 + self.d  # 200 + (self.d + 200)
                return 0
            self.d = self.d - 400  # -200 + (self.d - 200))
            return 1

    # 计算当前用户与边之间的信道增益
    # Deep Reinforcement Learning for Online Computation Offloading in Wireless Powered Mobile-Edge Computing Networks
    def get_channel_gain(self):
        # print("距离：       ", self.d)
        # print("方向：       ", self.d_direction)
        # print("移动速度：    ", self.d_speed)
        # print("信道衰落因子： ", self.fade_factor)
        return self.fade_factor * self.Ad * pow((3 * pow(10, 8) / (4 * np.pi * self.fc * abs(self.d))), self.de)
