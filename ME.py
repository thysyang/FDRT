from Environment_Wireless import EnvironmentWireless as EW
from Net import ME_Net
from Config import Config
import numpy as np
import torch.nn as nn
import torch
import os

# Hyper Parameters
BATCH_SIZE = 64
LR = 0.0004  # base learning rate
EPSILON = 0.9  # greedy policy
GAMMA = 0.9  # reward discount
TARGET_REPLACE_ITER = 100  # target update frequency
MEMORY_CAPACITY = 2000
N_STATES = 4  # states number
N_ACTIONS = 2

MAX_STEPS = 500
MAX_EPISODES = 100

# data_file = './data/data_test.csv'
loss_file = './data/data_loss.csv'
reward_file = './data/data_epoch_reward.csv'
episode_reward_file = './data/data_episode_reward.csv'
h_file = './data/data_h.csv'
first_episode_reward_file = './data/data_first_episode_reward.csv'
first_learning_episode_reward_file = './data/data_first_learning_episode_reward.csv'
last_learning_episode_reward_file = './data/data_last_learning_episode_reward.csv'

# 用于绘制曲线
Loss = [0 for i in range(MAX_STEPS)]
Reward = []
# 记录到每个episode累积reward的平均值
episode_reward = [0 for i in range(MAX_STEPS)]
first_episode_reward = [0 for i in range(MAX_STEPS)]
first_learning_episode_reward = [0 for i in range(MAX_STEPS)]
last_learning_episode_reward = [0 for i in range(MAX_STEPS)]
# 记录h信道增益变化
h_list = [0 for i in range(MAX_STEPS)]

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class ME_Agent:
    def __init__(self, edge_num):
        self.eval_net, self.target_net = ME_Net(N_STATES), ME_Net(N_STATES)
        # self.target_net.load_state_dict(self.eval_net.state_dict())

        self.learn_step_counter = 0  # for target updating
        self.memory_counter = 0  # for storing memory
        self.memory = np.zeros((MEMORY_CAPACITY, N_STATES * 2 + 2))  # initialize memory
        self.optimizer = torch.optim.Adam(self.eval_net.parameters(), lr=LR)
        self.loss_func = nn.MSELoss()

        self.edge_num = edge_num

    # 0-local     1-offload
    def choose_action(self, s):
        # increase channel gain to close to 1 for better training; it is a trick widely adopted in deep learning
        # decrease transmission data rate to close to 1 for better training
        s = torch.unsqueeze(torch.FloatTensor(s), 0)

        if np.random.random() < EPSILON:
            actions_value = self.eval_net.forward(s)
            action = torch.max(actions_value, 1)[1].numpy()[0]
        else:
            action = np.random.choice(N_ACTIONS)

        return action

    # transmission data rate   unit: bit/s    平均值为：185.5-6 Mbit/s  传输生成的数据平均需要1.88-89ms
    def get_transmission_data_rate(self, h):
        return Config.B * np.log2(1 + Config.tran_power * h / Config.noise)

    # 转换成状态，状态需要进行归一化处理
    def transform_state(self, h, data_size, cycle, queue_len, queue_cycle):
        return [(self.get_transmission_data_rate(h) - 185295277.8603332) / 82728673.22278965,  # 通信速度
                (data_size - 0.3) / 0.3,  # 数据大小
                (cycle - 9) / 5 * pow(3, 0.5),  # 数据每bit周期数
                queue_len,  # 队列任务长度
                queue_cycle]  # 队列总周期
        # (queue_cycle / queue_len - 2.7) / 8.79]  # 任务队列每个任务的平均周期

        # return [h * pow(10, 10), self.get_transmission_data_rate(h) / pow(10, 6)]

    def store_transition(self, s, a, r, s_):
        # s = self.transform_state(h)
        # s_ = self.transform_state(h_)
        transition = np.hstack((s, [a, r], s_))

        print("transition:\t", transition)

        # replace the old memory with new memory
        index = self.memory_counter % MEMORY_CAPACITY
        self.memory[index, :] = transition
        self.memory_counter += 1

    def learn(self, _episode, _step):
        # target parameter
        if self.learn_step_counter % TARGET_REPLACE_ITER == 0:
            self.target_net.load_state_dict(self.eval_net.state_dict())
        self.learn_step_counter += 1

        # sample batch transitions
        sample_index = np.random.choice(MEMORY_CAPACITY, BATCH_SIZE)
        b_memory = self.memory[sample_index, :]
        b_s = torch.FloatTensor(b_memory[:, :N_STATES])
        b_a = torch.LongTensor(b_memory[:, N_STATES:N_STATES + 1].astype(int))
        b_r = torch.FloatTensor(b_memory[:, N_STATES + 1:N_STATES + 2])
        b_s_ = torch.FloatTensor(b_memory[:, -N_STATES:])

        indices = np.arange(BATCH_SIZE)

        # q_eval w.r.t. the action in experience
        q_eval = self.eval_net(b_s)[indices, b_a]
        # q_eval = self.eval_net(b_s).gather(1, b_a)  # shape (batch, 1)
        q_next = self.target_net(b_s_).detach()
        # a_best = torch.max(self.eval_net.forward(b_s_), 1)[1].numpy()  # shape (batch, 1)
        a_best = torch.argmax(self.eval_net(b_s_), dim=1)
        q_target = b_r + GAMMA * q_next[indices, a_best]  # shape (batch, 1)
        loss = self.loss_func(q_eval, q_target)

        # 存入Loss数组
        Loss[_step] += loss.item()
        # Loss.append(loss.item())

        # 存入文件
        print("episode: ", _episode, "step: ", _step, "\tloss: ", loss.item(), "\n")

        # with open(data_file, 'a+') as file_object:
        #     file_object.write(str(i_episode))
        #     file_object.write("\t")
        #     file_object.write(str(loss.item()))
        #     file_object.write("\n")
        #     file_object.close()

        # 调整学习率   warm up
        #         if self.memory_counter - 3400 <= 0:
        #             for p in self.optimizer.param_groups:
        #                 p['lr'] = LR * ((self.memory_counter - 2000) / 1400)
        #         else:
        #             for p in self.optimizer.param_groups:
        #                 p['lr'] = p['lr'] ** 1.0001

        # 先将梯度归零（optimizer.zero_grad()），然后反向传播计算得到每个参数的梯度值（loss.backward()），最后通过梯度下降执行一步参数更新（optimizer.step()）
        self.optimizer.zero_grad()
        loss.backward()

        print("grad: \n", self.eval_net.out.weight.grad)

        self.optimizer.step()

    def train(self):
        # 删除数据文件
        # if os.path.exists(data_file):
        #     os.remove(data_file)
        if os.path.exists(loss_file):
            os.remove(loss_file)
        if os.path.exists(reward_file):
            os.remove(reward_file)
        if os.path.exists(episode_reward_file):
            os.remove(episode_reward_file)
        if os.path.exists(h_file):
            os.remove(h_file)

        for _episode in range(MAX_EPISODES):
            ew = EW()
            h = ew.get_channel_gain()

            config = Config()
            data_size = config.get_data_size()
            cycle = config.get_cycle_per_bit()

            queue = []  # 移动端本地队列，存剩余需要计算的周期数
            queue_data_cycle = 0  # 移动端本地队列已有数据大小

            s = self.transform_state(h, data_size, cycle, len(queue), queue_data_cycle)

            accumulate_reward = 0

            # i_episode_reward = 0

            for _step in range(MAX_STEPS):
                h_list[_step] += h

                a = self.choose_action(s)

                # 开始计算下一状态
                data_cycle = data_size * cycle  # 计算数据所需的CPU周期  单位：MCycle
                # 如果在本地计算，就把数据加到队列中
                if a == 0:
                    queue_data_cycle += data_cycle
                    queue.append(data_cycle)

                # change distance between user and edge
                ew.move()
                h_ = ew.get_channel_gain()
                data_size_ = config.get_data_size()
                cycle_ = config.get_cycle_per_bit()
                s_ = self.transform_state(h_, data_size_, cycle_, len(queue), queue_data_cycle)
                # 结束计算下一状态

                # action : 0-local     1-offload
                # 根据传输速度计算20ms可传输的数据量大
                tran_rate = self.get_transmission_data_rate(h) / pow(10, 6)
                # 1. 在边与用户20ms可传输数据量 < 用户生成数据量
                #       若a = 1，则r = -1.3
                #       若a = 0，则r = 0.8
                # 2. 在边缘计算时间 + 传输生成数据时间 > 本地计算时间
                #        若a = 1，则r = -1
                #        若a = 0，则r = 0.5
                # 3. 其他情况
                #        若a = 1，则r = 1
                #        若a = 0，则r = -2
                # 在1和2都采取a = 0收益不同原因，因为1是完全不能进行计算卸载，2是可以满足传输条件，但是传输时间太长，所以在1的情况下，采取a = 0获得收益更高，相应的惩罚也更大
                # 在3中a = 0收益负值比较大，这是因为系统鼓励，或者说更偏向进行计算卸载
                # 在1，2中正收益比较负收益小的原因也是系统鼓励，或者说更偏向进行计算卸载

                if data_size > (tran_rate * 0.02):
                    if a == 1:
                        # r = -1.3
                        r = -14
                    else:
                        # r = 0.8
                        r = 9
                elif (data_size / tran_rate + data_cycle / Config.Edge_CPU) > (data_cycle /
                                                                               Config.ME_CPU):  # unit: s
                    if a == 1:
                        # r = -1
                        r = -9
                    else:
                        # r = 0.5
                        r = 6
                else:
                    if a == 1:
                        # r = 1
                        r = 30
                    else:
                        # r = -2
                        r = -30

                # 计算每个epoch累积的Reward
                accumulate_reward += r

                # 将每个episode的reward都累积起来
                # i_episode_reward += r
                # episode_reward.append(i_episode_reward)
                episode_reward[_step] += r
                if _episode == 0:
                    first_episode_reward[_step] = r
                if _episode == 4:
                    first_learning_episode_reward[_step] = r
                if _episode == 99:
                    last_learning_episode_reward[_step] = r

                self.store_transition(s, a, r, s_)

                s = s_

                calculate_cycle = config.ME_CPU * 0.02
                queue_data_cycle = queue_data_cycle - calculate_cycle if queue_data_cycle > calculate_cycle else 0
                for i in range(len(queue)):
                    if queue[i] > calculate_cycle:
                        queue[i] -= calculate_cycle
                        index = i
                        break
                    calculate_cycle -= queue[i]
                queue = queue[index:]

                if self.memory_counter > MEMORY_CAPACITY:
                    self.learn(_episode, _step)

            Reward.append(accumulate_reward)

        torch.save(torch.tensor(Loss) / MAX_EPISODES, loss_file)
        torch.save(torch.tensor(Reward), reward_file)
        torch.save(torch.tensor(episode_reward) / MAX_EPISODES, episode_reward_file)
        torch.save(torch.tensor(h_list) / MAX_EPISODES, h_file)
        torch.save(torch.tensor(first_episode_reward), first_episode_reward_file)
        torch.save(torch.tensor(first_learning_episode_reward), first_learning_episode_reward_file)
        torch.save(torch.tensor(last_learning_episode_reward), last_learning_episode_reward_file)
