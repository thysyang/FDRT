# encoding: utf-8

from Environment_Wireless import EnvironmentWireless as EW
from Net import ME_Net
import Config as config

import numpy as np
import torch.nn as nn
import torch
import os

# Hyper Parameters
BATCH_SIZE = 64
LR = 0.0004  # base learning rate
EPSILON = 0.9  # greedy policy
GAMMA = 0.9  # reward discount
TARGET_REPLACE_ITER = 100  # target update frequency
MEMORY_CAPACITY = 2000
N_STATES = 5  # states number
N_ACTIONS = 2

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class Queue:
    def __init__(self):
        self.q = []  # 本地队列，存剩余需要计算的周期数
        self.cycle = 0  # 本地队列已有需计算周期大小

    def add(self, data_size, cycle):
        data_cycle = data_size * cycle
        self.q.append(data_cycle)
        self.cycle += data_cycle

    def reset(self):
        self.q = []
        self.cycle = 0

    def calculate(self, cpu):
        calculate_cycle = cpu * 0.02

        if self.cycle > calculate_cycle:
            self.cycle = self.cycle - calculate_cycle
            for i in range(len(self.q)):
                if self.q[i] > calculate_cycle:
                    self.q[i] -= calculate_cycle
                    self.q = self.q[i:]
                    return
                calculate_cycle -= self.q[i]
        else:
            self.q = []
            self.cycle = 0

    def print(self):
        for i in self.q:
            print(i, end=' ')


class ReplayBuffer:
    def __init__(self, state_dim, mem_size):
        self.mem_size = mem_size
        self.mem_cntr = 0

        self.state_memory = np.zeros((self.mem_size, state_dim), dtype=np.float32)
        self.next_state_memory = np.zeros((self.mem_size, state_dim), dtype=np.float32)
        self.action_memory = np.zeros(self.mem_size, dtype=np.int64)
        self.reward_memory = np.zeros(self.mem_size, dtype=np.float32)

    def store_transition(self, state, action, reward, state_):
        idx = self.mem_cntr % self.mem_size

        self.state_memory[idx] = state
        self.next_state_memory[idx] = state_
        self.action_memory[idx] = action
        self.reward_memory[idx] = reward

        self.mem_cntr += 1

    def sample(self, batch_size):
        max_mem = min(self.mem_cntr, self.mem_size)
        batch = np.random.choice(max_mem, batch_size, replace=False)

        states = self.state_memory[batch]
        states_ = self.next_state_memory[batch]
        actions = self.action_memory[batch]
        rewards = self.reward_memory[batch]

        return states, actions, rewards, states_


class ME_Agent:
    def __init__(self, num, edge_agent, edge_agents):
        self.num = num
        self.edge_num = edge_agent.num  # 用来保存这个me的初始edge
        self.edge_agent = edge_agent
        self.edge_agents = edge_agents

        self.eval_net, self.target_net = ME_Net(N_STATES, N_ACTIONS), ME_Net(N_STATES, N_ACTIONS)

        self.learn_step_counter = 0  # for target updating
        self.memory = ReplayBuffer(N_STATES, MEMORY_CAPACITY)
        self.optimizer = torch.optim.Adam(self.eval_net.parameters(), lr=LR)
        self.loss_func = nn.MSELoss()

        self.ew = EW()

        self.queue = Queue()

        # 初始状态
        self.h = self.ew.get_channel_gain()
        self.data_size = config.get_data_size()
        self.cycle = config.get_cycle_per_bit()
        self.s = self.transform_state(self.h, self.data_size, self.cycle, len(self.queue.q), self.queue.cycle)

        self.reward = 0
        self.Reward = []
        self.reward_file = './data/edge_' + str(self.edge_agent.num) + '_me_' + str(self.num) + '_reward.csv'

        self.Loss = []
        self.loss = 0
        self.loss_file = './data/edge_' + str(self.edge_agent.num) + '_me_' + str(
            self.num) + '_loss.csv'

        self.local = 0  # 每个episode的本地计算动作
        self.Local = []
        self.local_file = './data/edge_' + str(self.edge_agent.num) + '_me_' + str(self.num) + '_local.csv'

        self.local_times = []
        self.local_time = []  # 每个episode的所有本地计算时间
        self.local_times_file = './data/edge_' + str(self.edge_agent.num) + '_me_' + str(self.num) + '_local_times.csv'
        self.offload_times = []
        self.offload_time = []  # 每个episode的所有计算卸载时间
        self.offload_times_file = './data/edge_' + str(self.edge_agent.num) + '_me_' + str(
            self.num) + '_offload_times.csv'

        self.times = []
        self.time = []
        self.times_file = './data/edge_' + str(self.edge_agent.num) + '_me_' + str(
            self.num) + '_times.csv'

        self.times_2_me_baseline_one_episode = []
        self.times_2_me_baseline_one_episode_file = './data/edge_' + str(self.edge_agent.num) + '_me_' + str(
            self.num) + '_2_me_baseline_one_episode_times.csv'

        self.times_2_edge_baseline_one_episode = []
        self.times_2_edge_baseline_one_episode_file = './data/edge_' + str(self.edge_agent.num) + '_me_' + str(
            self.num) + '_2_edge_baseline_one_episode_times.csv'

        self.times_2_one_agent_one_episode = []
        self.times_2_one_agent_one_episode_file = './data/edge_' + str(self.edge_agent.num) + '_me_' + str(
            self.num) + '_2_one_agent_one_episode_times.csv'

        self.times_2_fl_one_episode = []
        self.times_2_fl_one_episode_file = './data/edge_' + str(self.edge_agent.num) + '_me_' + str(
            self.num) + '_2_fl_one_episode_times.csv'

        self.w = 1  # 计算奖励的权重

        self.eval_net.to(device)
        self.target_net.to(device)

    def set_eval_net_weight(self, weight):
        self.eval_net.load_state_dict(weight)

    def set_target_net_weight(self, weight):
        self.target_net.load_state_dict(weight)

    # 0-local     1-offload
    def choose_action(self, s):
        # increase channel gain to close to 1 for better training; it is a trick widely adopted in deep learning
        # decrease transmission data rate to close to 1 for better training
        # s = torch.unsqueeze(torch.FloatTensor(s), 0)

        if np.random.random() < EPSILON:
            s = torch.FloatTensor(s).to(device)
            actions_value = self.eval_net(s)
            action = torch.argmax(actions_value).item()
        else:
            action = np.random.choice(N_ACTIONS)

        return action

    def choose_action_(self, s):
        s = torch.FloatTensor(s).to(device)
        actions_value = self.eval_net(s)
        action = torch.argmax(actions_value).item()
        return action

    # transmission data rate   unit: bit/s    平均值为：185.5-6 Mbit/s  传输生成的数据平均需要1.88-89ms
    def get_transmission_data_rate(self, h):
        return config.B * np.log2(1 + config.tran_power * h / config.noise)

    # 转换成状态，状态需要进行归一化处理
    def transform_state(self, h, data_size, cycle, queue_len, queue_cycle):
        return [(self.get_transmission_data_rate(h) - 185669112.78751695) / 83423915.50777851,  # 通信速度
                (data_size - 0.3) / 0.3,  # 数据大小
                (cycle - 9) / 5 * pow(3, 0.5),  # 数据每bit周期数
                queue_len,  # 队列任务长度
                queue_cycle]  # 队列总周期

    def store_transition(self, s, a, r, s_):
        self.memory.store_transition(s, a, r, s_)

    def learn(self):
        # if self.memory.mem_cntr < BATCH_SIZE:
        if self.memory.mem_cntr < self.memory.mem_size:
            return

        # target parameter
        if self.learn_step_counter % TARGET_REPLACE_ITER == 0:
            self.target_net.load_state_dict(self.eval_net.state_dict())

        # sample batch transitions
        states, actions, rewards, states_ = self.memory.sample(BATCH_SIZE)

        states = torch.tensor(states).to(device)
        rewards = torch.tensor(rewards).to(device)
        actions = torch.tensor(actions).to(device)
        states_ = torch.tensor(states_).to(device)

        indices = np.arange(BATCH_SIZE)

        q_eval = self.eval_net(states)[indices, actions]
        q_next = self.target_net(states_).detach()
        a_best = torch.argmax(self.eval_net(states_), dim=1)
        q_target = rewards + GAMMA * q_next[indices, a_best]
        loss = self.loss_func(q_eval, q_target)

        self.loss += loss.item()

        # 先将梯度归零（optimizer.zero_grad()），然后反向传播计算得到每个参数的梯度值（loss.backward()），最后通过梯度下降执行一步参数更新（optimizer.step()）
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        self.learn_step_counter += 1

    def train(self):
        a = self.choose_action(self.s)

        # 开始计算下一状态
        # action : 0-local     1-offload
        # 如果在本地计算，就把数据加到队列中
        if a == 0:
            self.queue.add(self.data_size, self.cycle)
            time = self.queue.cycle / config.ME_CPU  # 单位: s
            self.local_time.append(time)
            self.local += 1
        else:
            time_edge_cal = self.edge_agent.train(self.data_size, self.cycle)  # 任务卸载出去获得结果的时间
            time_tran = self.data_size * pow(10, 6) / self.get_transmission_data_rate(self.h)  # 单位: s
            time = time_edge_cal + time_tran
            self.offload_time.append(time)

        self.time.append(time)

        r = - time

        # change distance between user and edge
        out = self.ew.move()
        if out == 0:
            self.edge_agent = self.edge_agents[
                (self.edge_agent.num - 1 + len(self.edge_agents)) % len(self.edge_agents)]
            print('edge_' + str(self.edge_num) + '_me_' + str(self.num) + ' left in edge ' + str(self.edge_agent.num))
        elif out == 1:
            self.edge_agent = self.edge_agents[(self.edge_agent.num + 1) % len(self.edge_agents)]
            print('edge_' + str(self.edge_num) + '_me_' + str(self.num) + ' right in edge ' + str(self.edge_agent.num))

        h_ = self.ew.get_channel_gain()
        data_size_ = config.get_data_size()
        cycle_ = config.get_cycle_per_bit()
        self.queue.calculate(config.ME_CPU)
        s_ = self.transform_state(h_, data_size_, cycle_, len(self.queue.q), self.queue.cycle)
        # 结束计算下一状态

        # 计算每个episode累积的Reward
        self.reward += r

        self.store_transition(self.s, a, r, s_)
        self.s = s_
        self.h = h_
        self.data_size = data_size_
        self.cycle = cycle_

        self.learn()

    def run(self):
        a = self.choose_action_(self.s)

        # 开始计算下一状态
        # action : 0-local     1-offload
        # 如果在本地计算，就把数据加到队列中
        if a == 0:
            self.queue.add(self.data_size, self.cycle)
            time = self.queue.cycle / config.ME_CPU  # 单位: s
            self.local_time.append(time)
            self.local += 1
        else:
            time_edge_cal = self.edge_agent.run(self.data_size, self.cycle)  # 任务卸载出去获得结果的时间
            time_tran = self.data_size * pow(10, 6) / self.get_transmission_data_rate(self.h)  # 单位: s
            time = time_edge_cal + time_tran
            self.offload_time.append(time)

        self.time.append(time)

        # change distance between user and edge
        out = self.ew.move()
        if out == 0:
            self.edge_agent = self.edge_agents[
                (self.edge_agent.num - 1 + len(self.edge_agents)) % len(self.edge_agents)]
            print('edge_' + str(self.edge_num) + '_me_' + str(self.num) + ' left in edge ' + str(self.edge_agent.num))
        elif out == 1:
            self.edge_agent = self.edge_agents[(self.edge_agent.num + 1) % len(self.edge_agents)]
            print('edge_' + str(self.edge_num) + '_me_' + str(self.num) + ' right in edge ' + str(self.edge_agent.num))

        h_ = self.ew.get_channel_gain()
        data_size_ = config.get_data_size()
        cycle_ = config.get_cycle_per_bit()
        self.queue.calculate(config.ME_CPU)
        s_ = self.transform_state(h_, data_size_, cycle_, len(self.queue.q), self.queue.cycle)
        # 结束计算下一状态

        self.s = s_
        self.h = h_
        self.data_size = data_size_
        self.cycle = cycle_

    def print(self):
        print('edge_' + str(self.edge_num) + '_me_' + str(self.num) + ' now in edge ' + str(self.edge_agent.num))

    def reset(self):
        self.ew = EW()

        # 初始状态
        self.h = self.ew.get_channel_gain()
        self.data_size = config.get_data_size()
        self.cycle = config.get_cycle_per_bit()
        self.s = self.transform_state(self.h, self.data_size, self.cycle, len(self.queue.q), self.queue.cycle)

        self.queue.reset()
        self.reward = 0
        self.loss = 0
        self.local = 0
        self.time = []
        self.local_time = []
        self.offload_time = []

    def get_times(self):
        return self.times

    def store(self):
        self.Reward.append(self.reward)
        self.Loss.append(self.loss)
        self.Local.append(self.local)
        self.times.append(np.mean(self.time))
        self.local_times.append(np.mean(self.local_time))
        self.offload_times.append(np.mean(self.offload_time))

    def store_2_me_baseline_one_episode(self):
        self.times_2_me_baseline_one_episode = self.time

    def store_2_edge_baseline_one_episode(self):
        self.times_2_edge_baseline_one_episode = self.time

    def store_2_one_agent_one_episode(self):
        self.times_2_one_agent_one_episode = self.time

    def store_2_fl_one_episode(self):
        self.times_2_fl_one_episode = self.time

    def save(self):
        if os.path.exists(self.reward_file):
            os.remove(self.reward_file)
        torch.save(torch.tensor(self.Reward), self.reward_file)

        if os.path.exists(self.loss_file):
            os.remove(self.loss_file)
        torch.save(torch.tensor(self.Loss), self.loss_file)

        if os.path.exists(self.times_file):
            os.remove(self.times_file)
        torch.save(torch.tensor(self.times), self.times_file)

        if os.path.exists(self.local_times_file):
            os.remove(self.local_times_file)
        torch.save(torch.tensor(self.local_times), self.local_times_file)

        if os.path.exists(self.offload_times_file):
            os.remove(self.offload_times_file)
        torch.save(torch.tensor(self.offload_times), self.offload_times_file)

        if os.path.exists(self.local_file):
            os.remove(self.local_file)
        torch.save(torch.tensor(self.Local), self.local_file)

        if os.path.exists(self.times_2_me_baseline_one_episode_file):
            os.remove(self.times_2_me_baseline_one_episode_file)
        torch.save(torch.tensor(self.times_2_me_baseline_one_episode), self.times_2_me_baseline_one_episode_file)

        if os.path.exists(self.times_2_edge_baseline_one_episode_file):
            os.remove(self.times_2_edge_baseline_one_episode_file)
        torch.save(torch.tensor(self.times_2_edge_baseline_one_episode), self.times_2_edge_baseline_one_episode_file)

        if os.path.exists(self.times_2_one_agent_one_episode_file):
            os.remove(self.times_2_one_agent_one_episode_file)
        torch.save(torch.tensor(self.times_2_one_agent_one_episode), self.times_2_one_agent_one_episode_file)

        if os.path.exists(self.times_2_fl_one_episode_file):
            os.remove(self.times_2_fl_one_episode_file)
        torch.save(torch.tensor(self.times_2_fl_one_episode), self.times_2_fl_one_episode_file)
