# encoding: utf-8

from ME_Agent import Queue, ReplayBuffer
import ME_Agent
from Net import ME_Net
import Config as config

import numpy as np
import torch
import torch.nn as nn
import os

# Hyper Parameters
BATCH_SIZE = ME_Agent.BATCH_SIZE
LR = ME_Agent.LR
EPSILON = ME_Agent.EPSILON
GAMMA = ME_Agent.GAMMA
MEMORY_CAPACITY = ME_Agent.MEMORY_CAPACITY
TARGET_REPLACE_ITER = ME_Agent.TARGET_REPLACE_ITER
N_STATES = ME_Agent.N_STATES
N_ACTIONS = ME_Agent.N_ACTIONS

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class ME_Agent_copy:
    def __init__(self, me_agent, edge_agents, me_type):
        self.me_agent = me_agent

        self.edge_num = self.me_agent.edge_agent.num
        self.edge_agent = None
        self.edge_agents = edge_agents

        self.queue = Queue()

        # 初始状态
        self.s = None
        self.a = None
        self.r = None

        # to federated learning
        self.eval_net, self.target_net = ME_Net(N_STATES, N_ACTIONS), ME_Net(N_STATES, N_ACTIONS)
        self.target_net.load_state_dict(self.eval_net.state_dict())

        self.learn_step_counter = 0  # for target updating
        self.memory = ReplayBuffer(N_STATES, MEMORY_CAPACITY)
        self.optimizer = torch.optim.Adam(self.eval_net.parameters(), lr=LR)
        self.loss_func = nn.MSELoss()

        self.eval_net.to(device)
        self.target_net.to(device)

        self.times = []
        self.time = []
        self.times_file = './data/' + me_type + '_edge_' + str(self.edge_num) + '_me_' + str(
            self.me_agent.num) + '_times.csv'

        self.times_one_episode = []
        self.times_one_episode_file = './data/' + me_type + '_edge_' + str(self.edge_num) + '_me_' + str(
            self.me_agent.num) + '_one_episode_times.csv'

        self.Loss = []
        self.loss = 0
        self.loss_file = './data/' + me_type + '_edge_' + str(self.edge_num) + '_me_' + str(
            self.me_agent.num) + '_loss.csv'

        self.reward = 0
        self.Reward = []
        self.reward_file = './data/' + me_type + '_edge_' + str(self.edge_num) + '_me_' + str(
            self.me_agent.num) + '_reward.csv'

    # transmission data rate   unit: bit/s    平均值为：185.5-6 Mbit/s  传输生成的数据平均需要1.88-89ms
    def get_transmission_data_rate(self, h):
        return config.B * np.log2(1 + config.tran_power * h / config.noise)

    def reset(self):
        self.queue.reset()
        self.time = []
        self.loss = 0
        self.reward = 0

    def update(self):
        self.edge_num = self.me_agent.edge_agent.num

    def get_eval_net_weight(self):
        return self.eval_net.state_dict()

    def set_eval_net_weight(self, weight):
        self.eval_net.load_state_dict(weight)

    def get_target_net_weight(self):
        return self.target_net.state_dict()

    def set_target_net_weight(self, weight):
        self.target_net.load_state_dict(weight)

    def choose_action(self, s):
        if np.random.random() < EPSILON:
            s = torch.FloatTensor(s).to(device)
            actions_value = self.eval_net(s)
            action = torch.argmax(actions_value).item()
        else:
            action = np.random.choice(N_ACTIONS)
        return action

    def choose_action_(self, s):
        s = torch.FloatTensor(s).to(device)
        actions_value = self.eval_net(s)
        action = torch.argmax(actions_value).item()
        return action

    def transform_state(self):
        # 增加edge队列状态
        return self.me_agent.s[:3] + [len(self.queue.q),  # 本地队列总长度
                                      self.queue.cycle]  # 本地队列总周期

    def store_transition(self, s, a, r, s_):
        self.memory.store_transition(s, a, r, s_)

    def learn(self):
        # if self.memory.mem_cntr < BATCH_SIZE:
        if self.memory.mem_cntr < self.memory.mem_size:
            return

        if self.learn_step_counter % TARGET_REPLACE_ITER == 0:
            self.target_net.load_state_dict(self.eval_net.state_dict())

        # sample batch transitions
        states, actions, rewards, states_ = self.memory.sample(BATCH_SIZE)

        states = torch.tensor(states).to(device)
        rewards = torch.tensor(rewards).to(device)
        actions = torch.tensor(actions).to(device)
        states_ = torch.tensor(states_).to(device)

        indices = np.arange(BATCH_SIZE)

        q_eval = self.eval_net(states)[indices, actions]
        q_next = self.target_net(states_).detach()
        a_best = torch.argmax(self.eval_net(states_), dim=1)
        q_target = rewards + GAMMA * q_next[indices, a_best]
        loss = self.loss_func(q_eval, q_target)

        self.loss += loss.item()

        # 先将梯度归零（optimizer.zero_grad()），然后反向传播计算得到每个参数的梯度值（loss.backward()），最后通过梯度下降执行一步参数更新（optimizer.step()）
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        self.learn_step_counter += 1

    # 用于附加的episode，只用网络选择动作
    def run(self):
        self.edge_agent = self.edge_agents[self.edge_num]

        self.s = self.transform_state()
        a = self.choose_action_(self.s)

        data_size = self.me_agent.data_size
        cycle = self.me_agent.cycle
        h = self.me_agent.h

        # 开始计算下一状态
        # action : 0-local     1-offload
        # 如果在本地计算，就把数据加到队列中
        if a == 0:
            self.queue.add(data_size, cycle)
            time = self.queue.cycle / config.ME_CPU  # 单位: s
        else:
            time_edge_cal = self.edge_agent.run(data_size, cycle)  # 任务卸载出去获得结果的时间
            time_tran = data_size * pow(10, 6) / self.get_transmission_data_rate(h)  # 单位: s
            time = time_edge_cal + time_tran

        self.time.append(time)
        self.queue.calculate(config.ME_CPU)

    def train1(self):
        self.edge_agent = self.edge_agents[self.edge_num]

        self.s = self.transform_state()
        a = self.choose_action(self.s)

        self.a = a
        data_size = self.me_agent.data_size
        cycle = self.me_agent.cycle
        h = self.me_agent.h

        # 开始计算下一状态
        # action : 0-local     1-offload
        # 如果在本地计算，就把数据加到队列中
        if a == 0:
            self.queue.add(data_size, cycle)
            time = self.queue.cycle / config.ME_CPU  # 单位: s
        else:
            time_edge_cal = self.edge_agent.train(data_size, cycle)  # 任务卸载出去获得结果的时间
            time_tran = data_size * pow(10, 6) / self.get_transmission_data_rate(h)  # 单位: s
            time = time_edge_cal + time_tran

        self.time.append(time)

        r = - time
        self.r = r
        self.reward += r

        self.queue.calculate(config.ME_CPU)

    def train2(self):
        self.update()
        s_ = self.transform_state()

        self.store_transition(self.s, self.a, self.r, s_)
        self.s = s_

        self.learn()

    def get_times(self):
        return self.times

    def store_one_episode(self):
        self.times_one_episode = self.time

    def store(self):
        self.times.append(np.mean(self.time))
        self.Loss.append(self.loss)
        self.Reward.append(self.reward)

    def save(self):
        if os.path.exists(self.times_file):
            os.remove(self.times_file)
        torch.save(torch.tensor(self.times), self.times_file)

        if os.path.exists(self.loss_file):
            os.remove(self.loss_file)
        torch.save(torch.tensor(self.Loss), self.loss_file)

        if os.path.exists(self.times_one_episode_file):
            os.remove(self.times_one_episode_file)
        torch.save(torch.tensor(self.times_one_episode), self.times_one_episode_file)

        if os.path.exists(self.reward_file):
            os.remove(self.reward_file)
        torch.save(torch.tensor(self.Reward), self.reward_file)
