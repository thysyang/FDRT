import ME_Agent
from ME_Agent import Queue
import Config as config

import numpy as np
import os
import torch


class ME_Random:
    def __init__(self, me_agent, edge_agents):
        self.me_agent = me_agent

        self.edge_agent = None
        self.edge_agents = edge_agents

        self.queue = Queue()

        self.times = []
        self.time = []
        self.times_file = './data/edge_' + str(self.me_agent.edge_agent.num) + '_me_' + str(
            self.me_agent.num) + '_only_random_times.csv'

        self.times_one_episode = []
        self.times_one_episode_file = './data/edge_' + str(self.me_agent.edge_agent.num) + '_me_' + str(
            self.me_agent.num) + '_one_episode_only_random_times.csv'

        self.reward = 0
        self.Reward = []
        self.reward_file = './data/edge_' + str(self.me_agent.edge_agent.num) + '_me_' + str(
            self.me_agent.num) + '_only_random_reward.csv'

    def reset(self):
        self.queue.reset()
        self.time = []
        self.reward = 0

    # 0-local     1-offload
    def choose_action(self):
        return np.random.choice(ME_Agent.N_ACTIONS)

    def train(self):
        self.edge_agent = self.edge_agents[self.me_agent.edge_agent.num]

        a = self.choose_action()

        if a == 0:
            self.queue.add(self.me_agent.data_size, self.me_agent.cycle)
            time = self.queue.cycle / config.ME_CPU  # 单位: s
        else:
            time_edge_cal = self.edge_agent.train(self.me_agent.data_size, self.me_agent.cycle)  # 任务卸载出去获得结果的时间
            time_tran = self.me_agent.data_size * pow(10, 6) / self.me_agent.get_transmission_data_rate(
                self.me_agent.h)  # 单位: s
            time = time_edge_cal + time_tran
        self.time.append(time)

        r = -time
        self.reward += r

        self.queue.calculate(config.ME_CPU)

    def get_times(self):
        return self.times

    def store(self):
        self.times.append(np.mean(self.time))
        self.Reward.append(self.reward)

    def store_one_episode(self):
        self.times_one_episode = self.time

    def save(self):
        if os.path.exists(self.times_file):
            os.remove(self.times_file)
        torch.save(torch.tensor(self.times), self.times_file)

        if os.path.exists(self.times_one_episode_file):
            os.remove(self.times_one_episode_file)
        torch.save(torch.tensor(self.times_one_episode), self.times_one_episode_file)

        if os.path.exists(self.reward_file):
            os.remove(self.reward_file)
        torch.save(torch.tensor(self.Reward), self.reward_file)
