import torch
import torch.nn as nn
import torch.nn.functional as F


class ME_Net(nn.Module):
    def __init__(self, state_dim, action_dim):
        super(ME_Net, self).__init__()
        self.fc1 = nn.Linear(state_dim, 128)
        # self.l1.weight.data.normal_(0, 0.1)  # initialization
        self.fc2 = nn.Linear(128, 64)
        # self.l2.weight.data.normal_(0, 0.1)  # initialization
        self.out = nn.Linear(64, action_dim)
        # self.out.weight.data.normal_(0, 0.1)  # initialization

    def forward(self, x):
        x = F.relu(self.fc1(x))  # relu max(0, x)
        x = F.relu(self.fc2(x))
        x = self.out(x)
        return x


class Edge_Net(nn.Module):
    def __init__(self, state_dim, action_dim):
        super(Edge_Net, self).__init__()

        self.fc1 = nn.Linear(state_dim, 128)
        self.fc2 = nn.Linear(128, 128)
        self.V = nn.Linear(128, 1)
        self.A = nn.Linear(128, action_dim)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))

        V = self.V(x)
        A = self.A(x)

        Q = V + (A - torch.mean(A, dim=1, keepdim=True))

        return Q

    def advantage(self, state):
        x = F.relu(self.fc1(state))
        x = F.relu(self.fc2(x))

        return self.A(x)
