# encoding: utf-8

import ME_Agent
from ME_Agent import Queue, ReplayBuffer
import Config as config
from Net import ME_Net

import numpy as np
import os
import torch
import torch.nn as nn

# Hyper Parameters
BATCH_SIZE = ME_Agent.BATCH_SIZE
LR = ME_Agent.LR  # base learning rate
EPSILON = ME_Agent.EPSILON  # greedy policy
GAMMA = ME_Agent.GAMMA  # reward discount
TARGET_REPLACE_ITER = ME_Agent.TARGET_REPLACE_ITER  # target update frequency
MEMORY_CAPACITY = ME_Agent.MEMORY_CAPACITY

N_STATES = ME_Agent.N_STATES + 2  # states number
N_ACTIONS = ME_Agent.N_ACTIONS + 1

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


# using Deep Q-Learning
class OneAgent:
    def __init__(self, me_agent, edge_setting):
        self.me_agent = me_agent
        self.edge_num = self.me_agent.edge_agent.num  # 仅用来查阅所属edge num
        self.edge_setting = edge_setting  # 仅用来查阅当前所属的edge队列情况

        self.queue = Queue()
        self.memory = ReplayBuffer(N_STATES, MEMORY_CAPACITY)
        self.loss = None

        self.learn_step_counter = 0  # for target updating
        self.eval_net, self.target_net = ME_Net(N_STATES, N_ACTIONS), ME_Net(N_STATES, N_ACTIONS)
        self.optimizer = torch.optim.Adam(self.eval_net.parameters(), lr=LR)
        self.loss_func = nn.MSELoss()

        self.s = self.transform_state()
        self.a = None
        self.r = None

        self.times = []
        self.time = []
        self.times_file = './data/edge_' + str(self.edge_num) + '_me_' + str(
            self.me_agent.num) + '_one_agent_times.csv'

        self.reward = 0
        self.Reward = []
        self.reward_file = './data/edge_' + str(self.edge_num) + '_me_' + str(
            self.me_agent.num) + '_one_agent_reward.csv'

        self.local = 0  # 每个episode的本地计算动作
        self.Local = []
        self.local_file = './data/edge_' + str(self.edge_num) + '_me_' + str(
            self.me_agent.num) + '_one_agent_local.csv'

        self.edge = 0  # 每个episode的edge动作
        self.Edge = []
        self.edge_file = './data/edge_' + str(self.edge_num) + '_me_' + str(
            self.me_agent.num) + '_one_agent_edge.csv'

        self.cloud = 0  # 每个episode的cloud动作
        self.Cloud = []
        self.cloud_file = './data/edge_' + str(self.edge_num) + '_me_' + str(
            self.me_agent.num) + '_one_agent_cloud.csv'

        self.times_one_episode = []
        self.times_one_episode_file = './data/edge_' + str(self.edge_num) + '_me_' + str(
            self.me_agent.num) + '_one_agent_one_episode_times.csv'

        self.eval_net.to(device)
        self.target_net.to(device)

    # 转换成状态，状态需要进行归一化处理
    def transform_state(self):
        # 增加edge队列状态
        return self.me_agent.s[:3] + [len(self.queue.q),  # 本地队列总长度
                                      self.queue.cycle,  # 本地队列总周期
                                      len(self.edge_setting.edge_task[self.edge_num].q),  # edge队列总长度
                                      self.edge_setting.edge_task[self.edge_num].cycle]  # edge队列总周期

    def store_transition(self, s, a, r, s_):
        self.memory.store_transition(s, a, r, s_)

    # 0-local     1-edge    2-cloud
    def choose_action(self, s):
        if np.random.random() < EPSILON:
            s = torch.FloatTensor(s).to(device)
            actions_value = self.eval_net(s)
            action = torch.argmax(actions_value).item()
        else:
            action = np.random.choice(N_ACTIONS)

        return action

    def learn(self):
        if self.memory.mem_cntr < self.memory.mem_size:
            return

        if self.learn_step_counter % TARGET_REPLACE_ITER == 0:
            self.target_net.load_state_dict(self.eval_net.state_dict())

        # sample batch transitions
        states, actions, rewards, states_ = self.memory.sample(BATCH_SIZE)

        states = torch.tensor(states).to(device)
        rewards = torch.tensor(rewards).to(device)
        actions = torch.tensor(actions).to(device)
        states_ = torch.tensor(states_).to(device)

        indices = np.arange(BATCH_SIZE)

        q_eval = self.eval_net(states)[indices, actions]
        q_next = self.target_net(states_).detach()
        a_best = torch.argmax(self.eval_net(states_), dim=1)
        q_target = rewards + GAMMA * q_next[indices, a_best]
        loss = self.loss_func(q_eval, q_target)
        self.loss = loss

        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        self.learn_step_counter += 1

    def train1(self):
        self.edge_num = self.me_agent.edge_agent.num

        self.a = self.choose_action(self.s)

        # 0-local     1-edge    2-cloud
        if self.a == 0:
            self.queue.add(self.me_agent.data_size, self.me_agent.cycle)
            time = self.queue.cycle / config.ME_CPU  # 单位: s
            self.local += 1
        elif self.a == 1:
            self.edge_setting.edge_task[self.edge_num].add(self.me_agent.data_size, self.me_agent.cycle)
            time_cal = self.edge_setting.edge_task[self.edge_num].cycle / config.Edge_CPU  # 单位: s
            time_tran = self.me_agent.data_size * pow(10, 6) / self.me_agent.get_transmission_data_rate(
                self.me_agent.h)  # 单位: s
            time = time_cal + time_tran
            self.edge += 1
        else:
            time_me2edge_tran = self.me_agent.data_size * pow(10, 6) / self.me_agent.get_transmission_data_rate(
                self.me_agent.h)
            # time_edge2cloud_tran = self.me_agent.data_size / config.Edge2Cloud_speed
            time_edge2cloud_tran = self.me_agent.data_size / self.edge_setting.edge2cloud_speed[self.edge_num]
            time = time_edge2cloud_tran + time_me2edge_tran
            self.cloud += 1

        self.r = - time

        self.reward += self.r

        self.time.append(time)

        self.queue.calculate(config.ME_CPU)

    def train2(self):
        s_ = self.transform_state()
        self.store_transition(self.s, self.a, self.r, s_)
        self.s = s_
        self.learn()

    def print(self):
        print(self.loss)

    def reset(self):
        self.queue.reset()
        self.time = []
        self.reward = 0
        self.local = 0
        self.edge = 0
        self.cloud = 0

    def get_times(self):
        return self.times

    def store(self):
        self.times.append(np.mean(self.time))
        self.Reward.append(self.reward)
        self.Local.append(self.local)
        self.Edge.append(self.edge)
        self.Cloud.append(self.cloud)

    def store_one_episode(self):
        self.times_one_episode = self.time

    def save(self):
        if os.path.exists(self.times_file):
            os.remove(self.times_file)
        torch.save(torch.tensor(self.times), self.times_file)

        if os.path.exists(self.reward_file):
            os.remove(self.reward_file)
        torch.save(torch.tensor(self.Reward), self.reward_file)

        if os.path.exists(self.local_file):
            os.remove(self.local_file)
        torch.save(torch.tensor(self.Local), self.local_file)

        if os.path.exists(self.edge_file):
            os.remove(self.edge_file)
        torch.save(torch.tensor(self.Edge), self.edge_file)

        if os.path.exists(self.cloud_file):
            os.remove(self.cloud_file)
        torch.save(torch.tensor(self.Cloud), self.cloud_file)

        if os.path.exists(self.times_one_episode_file):
            os.remove(self.times_one_episode_file)
        torch.save(torch.tensor(self.times_one_episode), self.times_one_episode_file)
