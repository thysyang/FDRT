# encoding: utf-8

import torch
import torch.optim as optim
import torch.nn as nn
import numpy as np
import os
import Config as config
from Net import ME_Net
from ME_Agent import ReplayBuffer
import Edge_Agent

# Hyper Parameters
BATCH_SIZE = Edge_Agent.BATCH_SIZE
LR = Edge_Agent.LR  # learning rate
EPSILON = Edge_Agent.EPSILON  # greedy policy
GAMMA = Edge_Agent.GAMMA  # reward discount\
TARGET_REPLACE_ITER = Edge_Agent.TARGET_REPLACE_ITER
MEMORY_CAPACITY = 2000

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class OneAgent_Edge:
    def __init__(self, me_agents, edge_setting):
        self.state_dim = (len(edge_setting.edge_task) + 1) * 2 + 3  # 所有edge队列状态 + 本地队列 + data size + data cycle + v
        self.action_dim = len(edge_setting.edge_task) + 2  # 所有edge + cloud + local

        self.memory = ReplayBuffer(self.state_dim, MEMORY_CAPACITY)
        # self.eval_net, self.target_net = \
        #     Edge_Net(self.state_dim, self.action_dim), Edge_Net(self.state_dim, self.action_dim)
        self.eval_net, self.target_net = \
            ME_Net(self.state_dim, self.action_dim), ME_Net(self.state_dim, self.action_dim)
        self.optim = optim.Adam(self.eval_net.parameters(), lr=LR)
        self.loss_func = nn.MSELoss()

        self.eval_net.to(device)
        self.target_net.to(device)

        self.learn_step_counter = 0  # for target updating

        self.setting = edge_setting
        self.me_agents = me_agents
        self.s = None
        self.a = None
        self.r = None
        self.loss = None

        self.reward = 0
        self.Reward = []
        self.reward_file = './data/one_agent_edge_reward.csv'

        self.times = []
        self.time = []
        self.times_file = './data/one_agent_edge_times.csv'

        self.local = 0  # 每个episode的本地计算动作
        self.Local = []
        self.local_file = './data/one_agent_edge_local.csv'

        self.edge = 0  # 每个episode的edge动作
        self.Edge = []
        self.edge_file = './data/one_agent_edge_edge.csv'

        self.cloud = 0  # 每个episode的cloud动作
        self.Cloud = []
        self.cloud_file = './data/one_agent_edge_cloud.csv'

        self.times_one_episode = []
        self.times_one_episode_file = './data/one_agent_edge_one_episode_times.csv'

    def transform_state(self, me_agent):
        state = [me_agent.me_agent.data_size, me_agent.me_agent.cycle, len(me_agent.queue.q), me_agent.queue.cycle,
                 me_agent.get_transmission_data_rate(me_agent.me_agent.h)]
        for i in range(config.Edge_num):
            state.append(len(self.setting.edge_task[i].q))
            state.append(self.setting.edge_task[i].cycle)
        return state

    def store_transition(self, s, a, r, s_):
        self.memory.store_transition(s, a, r, s_)

    # 0-me local, 1-cloud, other-edge
    def choose_action(self, s):
        if np.random.random() < EPSILON:
            # s = torch.FloatTensor(s).to(device)
            # advantage = self.eval_net.advantage(s)
            # action = torch.argmax(advantage).item()
            s = torch.FloatTensor(s).to(device)
            actions_value = self.eval_net(s)
            action = torch.argmax(actions_value).item()
        else:
            action = np.random.choice(self.action_dim)

        return action

    def learn(self):
        if self.memory.mem_cntr < self.memory.mem_size:
            return

        if self.learn_step_counter % TARGET_REPLACE_ITER == 0:
            self.target_net.load_state_dict(self.eval_net.state_dict())

        states, actions, rewards, states_ = self.memory.sample(BATCH_SIZE)

        states = torch.tensor(states).to(device)
        rewards = torch.tensor(rewards).to(device)
        actions = torch.tensor(actions).to(device)
        states_ = torch.tensor(states_).to(device)

        indices = np.arange(BATCH_SIZE)

        q_eval = self.eval_net(states)[indices, actions]
        q_next = self.target_net(states_)
        a_best = torch.argmax(self.eval_net(states_), dim=1)
        q_target = rewards + GAMMA * q_next[indices, a_best]

        self.optim.zero_grad()

        loss = self.loss_func(q_target, q_eval)
        self.loss = loss
        loss.backward()

        self.optim.step()

        self.learn_step_counter += 1

    def print(self):
        print(self.loss)

    def train_every1(self, me_agent):
        data_size = me_agent.me_agent.data_size
        cycle = me_agent.me_agent.cycle
        h = me_agent.me_agent.h
        self.s = self.transform_state(me_agent)

        a = self.choose_action(self.s)
        self.a = a
        if a == 0:  # me local
            me_agent.queue.add(data_size, cycle)
            time = me_agent.queue.cycle / config.ME_CPU  # 单位: s
            self.local += 1
        elif a == 1:  # cloud
            time_me2edge_tran = data_size * pow(10, 6) / me_agent.get_transmission_data_rate(h)
            # time_edge2cloud_tran = data_size / config.Edge2Cloud_speed
            time_edge2cloud_tran = data_size / self.setting.edge2cloud_speed[me_agent.edge_num]
            time = time_edge2cloud_tran + time_me2edge_tran
            self.cloud += 1
        else:
            self.setting.edge_task[a - 2].add(data_size, cycle)
            time_me2edge_tran = data_size * pow(10, 6) / me_agent.get_transmission_data_rate(
                h)
            time_cal = self.setting.edge_task[a - 2].cycle / config.Edge_CPU
            time = time_me2edge_tran + time_cal
            if me_agent.edge_num != a - 2:
                # time += data_size / config.Edges_speed
                if me_agent.edge_num > a - 2:
                    for i in range(a - 2, me_agent.edge_num):
                        time += data_size / self.setting.edges_speed[i][i + 1]
                else:
                    for i in range(me_agent.edge_num, a - 2):
                        time += data_size / self.setting.edges_speed[i][i + 1]
            self.edge += 1

        r = - time
        self.r = r

        self.time.append(time)
        # 计算每个episode累积的Reward
        self.reward += r
        me_agent.queue.calculate(config.ME_CPU)

    def train_every2(self, me_agent):
        s_ = self.transform_state(me_agent)
        self.store_transition(self.s, self.a, self.r, s_)
        self.s = s_
        self.learn()

    def train1(self):
        for me_agent in self.me_agents:
            self.train_every1(me_agent)

    def train2(self):
        self.setting.calculate()
        for me_agent in self.me_agents:
            me_agent.update()
            self.train_every2(me_agent)

    def reset(self):
        for me_agent in self.me_agents:
            me_agent.reset()
        self.setting.reset()
        self.reward = 0
        self.time = []
        self.local = 0
        self.edge = 0
        self.cloud = 0

    def get_times(self):
        return self.times

    def store(self):
        self.Reward.append(self.reward)
        self.times.append(np.mean(self.time))
        self.Local.append(self.local)
        self.Edge.append(self.edge)
        self.Cloud.append(self.cloud)

    def store_one_episode(self):
        self.times_one_episode = self.time

    def save(self):
        if os.path.exists(self.reward_file):
            os.remove(self.reward_file)
        torch.save(torch.tensor(self.Reward), self.reward_file)

        if os.path.exists(self.times_file):
            os.remove(self.times_file)
        torch.save(torch.tensor(self.times), self.times_file)

        if os.path.exists(self.local_file):
            os.remove(self.local_file)
        torch.save(torch.tensor(self.Local), self.local_file)

        if os.path.exists(self.edge_file):
            os.remove(self.edge_file)
        torch.save(torch.tensor(self.Edge), self.edge_file)

        if os.path.exists(self.cloud_file):
            os.remove(self.cloud_file)
        torch.save(torch.tensor(self.Cloud), self.cloud_file)

        if os.path.exists(self.times_one_episode_file):
            os.remove(self.times_one_episode_file)
        torch.save(torch.tensor(self.times_one_episode), self.times_one_episode_file)
