from ME_Agent import ME_Agent
from Edge_Agent import Edge_Agent_common, Edge_Agent
import Config as config
from ME_Local import ME_Local
from ME_Offload import ME_Offload
from ME_Random import ME_Random
from OneAgent import OneAgent
from OneAgent_Edge import OneAgent_Edge
from ME_Agent_copy import ME_Agent_copy
from Edge_Local import Edge_Local
from Edge_Cloud import Edge_Cloud
from Edge_Random import Edge_Random

import time
import os
import numpy as np
import torch

MAX_EPISODES = 200
MAX_STEPS = 200

# fl aggregation frequency
aggregation_frequency = 100

edge_agents = []
me_agents = []

# my federated learning
edge_agents_fl = []
me_agents_fl = []

# traditional federated learning
edge_agents_fl_td = []
me_agents_fl_td = []

# me only local
me_agents_local = []

# me only offload
edge_agents_me_offload = []
me_agents_offload = []

# me random
edge_agents_me_random = []
me_agents_random = []

# one agent in me
one_agents = []

# one agent in edge
me_agents_copy2edge = []
one_agent_edge = []

# edge local
edge_agents_local = []
me_agents_edge_local = []

# edge cloud
edge_agents_cloud = []
me_agents_edge_cloud = []

# edge random
edge_agents_random = []
me_agents_edge_random = []

# edge_num = 6
adjacent_matrix = [[0, 1, 0, 0, 0, 1],
                   [1, 0, 1, 0, 0, 0],
                   [0, 1, 0, 1, 0, 0],
                   [0, 0, 1, 0, 1, 0],
                   [0, 0, 0, 1, 0, 1],
                   [1, 0, 0, 0, 1, 0]]
edges_speed = np.zeros((config.Edge_num, config.Edge_num))
edge2cloud_speed = np.zeros(config.Edge_num)
# edge_task = [Queue() for i in range(config.Edge_num)]  # 每个edge的task个数以及总cycle

edge_setting = Edge_Agent_common(adjacent_matrix, edges_speed, edge2cloud_speed)
edge_me_offload_setting = Edge_Agent_common(adjacent_matrix, edges_speed, edge2cloud_speed)
edge_me_random_setting = Edge_Agent_common(adjacent_matrix, edges_speed, edge2cloud_speed)
one_agent_setting = Edge_Agent_common(adjacent_matrix, edges_speed, edge2cloud_speed)
one_agent_edge_setting = Edge_Agent_common(adjacent_matrix, edges_speed, edge2cloud_speed)
edge_fl_setting = Edge_Agent_common(adjacent_matrix, edges_speed, edge2cloud_speed)
edge_fl_td_setting = Edge_Agent_common(adjacent_matrix, edges_speed, edge2cloud_speed)

edge_local_setting = Edge_Agent_common(adjacent_matrix, edges_speed, edge2cloud_speed)
edge_cloud_setting = Edge_Agent_common(adjacent_matrix, edges_speed, edge2cloud_speed)
edge_random_setting = Edge_Agent_common(adjacent_matrix, edges_speed, edge2cloud_speed)


def build_agent():
    for i in range(config.Edge_num):
        edge_agent = Edge_Agent(i, np.sum(adjacent_matrix[i]), edge_setting, 'prime')
        edge_agents.append(edge_agent)

        edge_agent_me_offload = Edge_Agent(i, np.sum(adjacent_matrix[i]), edge_me_offload_setting, 'offload')
        edge_agents_me_offload.append(edge_agent_me_offload)
        edge_agent_me_random = Edge_Agent(i, np.sum(adjacent_matrix[i]), edge_me_random_setting, 'random')
        edge_agents_me_random.append(edge_agent_me_random)

        edge_agent_fl = Edge_Agent(i, np.sum(adjacent_matrix[i]), edge_fl_setting, 'fl')
        edge_agents_fl.append(edge_agent_fl)
        edge_agent_fl_td = Edge_Agent(i, np.sum(adjacent_matrix[i]), edge_fl_td_setting, 'fl_td')
        edge_agents_fl_td.append(edge_agent_fl_td)

        edge_agent_local = Edge_Local(i, edge_local_setting, 'edge_local')
        edge_agents_local.append(edge_agent_local)
        edge_agent_cloud = Edge_Cloud(i, edge_cloud_setting, 'edge_cloud')
        edge_agents_cloud.append(edge_agent_cloud)
        edge_agent_random = Edge_Random(i, np.sum(adjacent_matrix[i]), edge_random_setting, 'edge_random')
        edge_agents_random.append(edge_agent_random)

        if i & 1 == 1:
            continue

        for j in range(config.ME_num):
            me_agent = ME_Agent(j, edge_agent, edge_agents)
            me_agents.append(me_agent)

            me_agents_local.append(ME_Local(me_agent))
            me_agents_offload.append(ME_Offload(me_agent, edge_agents_me_offload))
            me_agents_random.append(ME_Random(me_agent, edge_agents_me_random))

            one_agents.append(OneAgent(me_agent, one_agent_setting))

            me_agents_copy2edge.append(ME_Agent_copy(me_agent, None, 'one_edge_agent'))

            me_agents_fl.append(ME_Agent_copy(me_agent, edge_agents_fl, 'fl'))
            me_agents_fl_td.append(ME_Agent_copy(me_agent, edge_agents_fl_td, 'fl_td'))

            me_agents_edge_local.append(ME_Agent_copy(me_agent, edge_agents_local, 'edge_local'))
            me_agents_edge_cloud.append(ME_Agent_copy(me_agent, edge_agents_cloud, 'edge_cloud'))
            me_agents_edge_random.append(ME_Agent_copy(me_agent, edge_agents_random, 'edge_random'))

    one_agent_edge.append(OneAgent_Edge(me_agents_copy2edge, one_agent_edge_setting))

    # 初始化 fl 所有网络参数相同
    for i in range(len(me_agents_fl)):
        if i % config.ME_num == 0:
            weight0 = me_agents_fl[i].get_eval_net_weight()
            weight1 = me_agents_fl[i].get_target_net_weight()
        me_agents[i].set_eval_net_weight(weight0)
        me_agents[i].set_target_net_weight(weight1)
        me_agents_fl[i].set_eval_net_weight(weight0)
        me_agents_fl[i].set_target_net_weight(weight1)
        me_agents_fl_td[i].set_eval_net_weight(weight0)
        me_agents_fl_td[i].set_target_net_weight(weight1)


def reset():
    for i in range(config.Edge_num):
        e2c_speed = config.get_edge2cloud_speed()
        edge2cloud_speed[i] = e2c_speed
        for j in range(i, config.Edge_num):
            if adjacent_matrix[i][j] == 1:
                edge_speed = config.get_edges_speed()
                edges_speed[i][j] = edge_speed
                edges_speed[j][i] = edge_speed

    for me_agent in me_agents:
        me_agent.reset()
    edge_setting.reset()
    for edge_agent in edge_agents:
        edge_agent.reset()

    for me_agent_local in me_agents_local:
        me_agent_local.reset()
    for me_agent_offload in me_agents_offload:
        me_agent_offload.reset()
    edge_me_offload_setting.reset()
    for edge_agent_me_offload in edge_agents_me_offload:
        edge_agent_me_offload.reset()
    for me_agent_random in me_agents_random:
        me_agent_random.reset()
    edge_me_random_setting.reset()
    for edge_agent_me_random in edge_agents_me_random:
        edge_agent_me_random.reset()

    for one_agent in one_agents:
        one_agent.reset()
    one_agent_setting.reset()

    one_agent_edge[0].reset()

    for me_agent_fl in me_agents_fl:
        me_agent_fl.reset()
    edge_fl_setting.reset()
    for edge_agent_fl in edge_agents_fl:
        edge_agent_fl.reset()
    for me_agent_fl_td in me_agents_fl_td:
        me_agent_fl_td.reset()
    edge_fl_td_setting.reset()
    for edge_agent_fl_td in edge_agents_fl_td:
        edge_agent_fl_td.reset()

    for me_agent_edge_local in me_agents_edge_local:
        me_agent_edge_local.reset()
    edge_local_setting.reset()
    for edge_agent_local in edge_agents_local:
        edge_agent_local.reset()
    for me_agent_edge_cloud in me_agents_edge_cloud:
        me_agent_edge_cloud.reset()
    edge_cloud_setting.reset()
    for edge_agent_cloud in edge_agents_cloud:
        edge_agent_cloud.reset()
    for me_agent_edge_random in me_agents_edge_random:
        me_agent_edge_random.reset()
    edge_local_setting.reset()
    for edge_agent_random in edge_agents_random:
        edge_agent_random.reset()


def reset_():
    for i in range(config.Edge_num):
        e2c_speed = config.get_edge2cloud_speed()
        edge2cloud_speed[i] = e2c_speed
        for j in range(i, config.Edge_num):
            if adjacent_matrix[i][j] == 1:
                edge_speed = config.get_edges_speed()
                edges_speed[i][j] = edge_speed
                edges_speed[j][i] = edge_speed

    for me_agent in me_agents:
        me_agent.reset()
    edge_setting.reset()
    for edge_agent in edge_agents:
        edge_agent.reset()

    for me_agent_fl in me_agents_fl:
        me_agent_fl.reset()
    edge_fl_setting.reset()
    for edge_agent_fl in edge_agents_fl:
        edge_agent_fl.reset()

    for me_agent_fl_td in me_agents_fl_td:
        me_agent_fl_td.reset()
    edge_fl_td_setting.reset()
    for edge_agent_fl_td in edge_agents_fl_td:
        edge_agent_fl_td.reset()


def train(c):
    edge_mes = [[] for i in range(config.Edge_num)]

    for me_agent_local in me_agents_local:
        me_agent_local.train()
    for me_agent_offload in me_agents_offload:
        me_agent_offload.train()
    for me_agent_random in me_agents_random:
        me_agent_random.train()

    one_agent_edge[0].train1()

    for i in range(len(me_agents)):
        one_agents[i].train1()
        me_agents_fl[i].train1()
        me_agents_fl_td[i].train1()
        me_agents_edge_local[i].train1()
        me_agents_edge_cloud[i].train1()
        me_agents_edge_random[i].train1()
        me_agents[i].train()
    one_agent_setting.calculate()
    for i in range(len(me_agents)):
        one_agents[i].train2()
        me_agents_fl[i].train2()
        me_agents_fl_td[i].train2()
        me_agents_edge_local[i].train2()
        me_agents_edge_cloud[i].train2()
        me_agents_edge_random[i].train2()
        edge_mes[me_agents_fl[i].edge_num].append(me_agents_fl[i])

    one_agent_edge[0].train2()

    edge_setting.calculate()

    edge_me_offload_setting.calculate()
    edge_me_random_setting.calculate()

    edge_fl_setting.calculate()
    edge_fl_td_setting.calculate()

    edge_local_setting.calculate()
    edge_cloud_setting.calculate()
    edge_random_setting.calculate()

    if c > 2000:
        if c % aggregation_frequency == 0:
            aggregation(edge_mes)


def train_():
    for i in range(len(me_agents)):
        me_agents_fl[i].run()
        me_agents_fl_td[i].run()

        me_agents[i].run()

        me_agents_fl[i].update()
        me_agents_fl_td[i].update()

    edge_setting.calculate()
    edge_fl_setting.calculate()
    edge_fl_td_setting.calculate()


def aggregation(edge_mes):
    # my fl
    edge_mes_weight = []
    for edge_me in edge_mes:
        if len(edge_me) == 0:
            edge_mes_weight.append(None)
            continue

        sum_weight = None
        for me in edge_me:
            if sum_weight is None:
                sum_weight = me.get_eval_net_weight()
            else:
                weight = me.get_eval_net_weight()
                for var in sum_weight:
                    sum_weight[var] = sum_weight[var] + weight[var]

        edge_mes_weight.append(sum_weight)

    for i in range(len(edge_mes)):
        if len(edge_mes[i]) == 0:
            continue

        num = len(edge_mes[i])
        weight = edge_mes_weight[i].copy()  # 防止修改list中元素
        left = (i - 1 + config.Edge_num) % config.Edge_num
        right = (i + 1) % config.Edge_num

        if len(edge_mes[left]) != 0 and len(edge_mes[right]) != 0:
            left_num = len(edge_mes[left])
            right_num = len(edge_mes[right])
            sum_num = num + left_num + right_num
            for var in weight:
                weight[var] = (weight[var] + edge_mes_weight[left][var] + edge_mes_weight[right][var]) / sum_num
        elif len(edge_mes[left]) != 0:
            left_num = len(edge_mes[left])
            sum_num = num + left_num
            for var in weight:
                weight[var] = (weight[var] + edge_mes_weight[left][var]) / sum_num
        elif len(edge_mes[right]) != 0:
            right_num = len(edge_mes[right])
            sum_num = num + right_num
            for var in weight:
                weight[var] = (weight[var] + edge_mes_weight[right][var]) / sum_num
        else:
            for var in weight:
                weight[var] = weight[var] / num

        for me in edge_mes[i]:
            me.set_eval_net_weight(weight)

    # traditional fl
    sum_weight = None
    for me_agent_fl_td in me_agents_fl_td:
        if sum_weight is None:
            sum_weight = me_agent_fl_td.get_eval_net_weight()
        else:
            weight = me_agent_fl_td.get_eval_net_weight()
            for var in sum_weight:
                sum_weight[var] = sum_weight[var] + weight[var]

    for var in sum_weight:
        sum_weight[var] = sum_weight[var] / len(me_agents_fl_td)

    for me_agent_fl_td in me_agents_fl_td:
        me_agent_fl_td.set_eval_net_weight(sum_weight)


def store():
    for me_agent in me_agents:
        me_agent.store()
    for edge_agent in edge_agents:
        edge_agent.store()

    for me_agent_local in me_agents_local:
        me_agent_local.store()
    for me_agent_offload in me_agents_offload:
        me_agent_offload.store()
    for edge_agent_me_offload in edge_agents_me_offload:
        edge_agent_me_offload.store()
    for me_agent_random in me_agents_random:
        me_agent_random.store()
    for edge_agent_me_random in edge_agents_me_random:
        edge_agent_me_random.store()

    for one_agent in one_agents:
        one_agent.store()

    one_agent_edge[0].store()

    for me_agent_fl in me_agents_fl:
        me_agent_fl.store()
    for edge_agent_fl in edge_agents_fl:
        edge_agent_fl.store()
    for me_agent_fl_td in me_agents_fl_td:
        me_agent_fl_td.store()
    for edge_agent_fl_td in edge_agents_fl_td:
        edge_agent_fl_td.store()

    for me_agent_edge_local in me_agents_edge_local:
        me_agent_edge_local.store()
    for edge_agent_local in edge_agents_local:
        edge_agent_local.store()
    for me_agent_edge_cloud in me_agents_edge_cloud:
        me_agent_edge_cloud.store()
    for edge_agent_cloud in edge_agents_cloud:
        edge_agent_cloud.store()
    for me_agent_edge_random in me_agents_edge_random:
        me_agent_edge_random.store()
    for edge_agent_random in edge_agents_random:
        edge_agent_random.store()


def store_():
    for me_agent in me_agents:
        me_agent.store()
    for edge_agent in edge_agents:
        edge_agent.store()

    for me_agent_fl in me_agents_fl:
        me_agent_fl.store()
    for edge_agent_fl in edge_agents_fl:
        edge_agent_fl.store()

    for me_agent_fl_td in me_agents_fl_td:
        me_agent_fl_td.store()
    for edge_agent_fl_td in edge_agents_fl_td:
        edge_agent_fl_td.store()


def store_me_baseline_one_episode():
    for me_agent in me_agents:
        me_agent.store_2_me_baseline_one_episode()

    for me_agent_local in me_agents_local:
        me_agent_local.store_one_episode()
    for me_agent_offload in me_agents_offload:
        me_agent_offload.store_one_episode()
    for me_agent_random in me_agents_random:
        me_agent_random.store_one_episode()


def me_baseline_one_episode(diff, epi, _epi):
    times_me = []
    times_only_offload = []

    for me_agent in me_agents:
        times_me.append(me_agent.get_times()[-1])

    for me_agent_offload in me_agents_offload:
        times_only_offload.append(me_agent_offload.get_times()[-1])

    times_diff = np.nanmean(times_only_offload) - np.nanmean(times_me)
    if diff[0] < times_diff:
        diff[0] = times_diff
        epi[0] = _epi
        store_me_baseline_one_episode()


def store_edge_baseline_one_episode():
    for me_agent in me_agents:
        me_agent.store_2_edge_baseline_one_episode()

    for me_agent_edge_local in me_agents_edge_local:
        me_agent_edge_local.store_one_episode()
    for me_agent_edge_cloud in me_agents_edge_cloud:
        me_agent_edge_cloud.store_one_episode()
    for me_agent_edge_random in me_agents_edge_random:
        me_agent_edge_random.store_one_episode()


def edge_baseline_one_episode(diff, epi, _epi):
    times_me = []
    times_edge_local = []

    for me_agent in me_agents:
        times_me.append(me_agent.get_times()[-1])

    for me_agent_edge_local in me_agents_edge_local:
        times_edge_local.append(me_agent_edge_local.get_times()[-1])

    times_diff = np.nanmean(times_edge_local) - np.nanmean(times_me)
    if diff[1] < times_diff:
        diff[1] = times_diff
        epi[1] = _epi
        store_edge_baseline_one_episode()


def store_one_agent_one_episode():
    for me_agent in me_agents:
        me_agent.store_2_one_agent_one_episode()

    for one_agent in one_agents:
        one_agent.store_one_episode()

    one_agent_edge[0].store_one_episode()


def one_agent_one_episode(diff, epi, _epi):
    times_me = []
    times_one_agent = []

    for me_agent in me_agents:
        times_me.append(me_agent.get_times()[-1])

    for one_agent in one_agents:
        times_one_agent.append(one_agent.get_times()[-1])

    times_diff = np.nanmean(times_one_agent) - np.nanmean(times_me)
    if diff[2] < times_diff:
        diff[2] = times_diff
        epi[2] = _epi
        store_one_agent_one_episode()


def store_fl_one_episode():
    for me_agent in me_agents:
        me_agent.store_2_fl_one_episode()

    for me_agent_fl in me_agents_fl:
        me_agent_fl.store_one_episode()
    for me_agent_fl_td in me_agents_fl_td:
        me_agent_fl_td.store_one_episode()


def fl_one_episode(diff, epi, _epi):
    times_me = []
    times_fl = []

    for me_agent in me_agents:
        times_me.append(me_agent.get_times()[-1])

    for me_agent_fl in me_agents_fl:
        times_fl.append(me_agent_fl.get_times()[-1])

    times_diff = np.nanmean(times_fl) - np.nanmean(times_me)
    if diff[3] < times_diff:
        diff[3] = times_diff
        epi[3] = _epi
        store_fl_one_episode()


def save(episode_worst):
    if not os.path.exists('./data'):
        os.mkdir('./data')
    else:
        os.system('rm -f data/*')

    for me_agent in me_agents:
        me_agent.save()
    for edge_agent in edge_agents:
        edge_agent.save()

    for me_agent_local in me_agents_local:
        me_agent_local.save()
    for me_agent_offload in me_agents_offload:
        me_agent_offload.save()
    for edge_agent_me_offload in edge_agents_me_offload:
        edge_agent_me_offload.save()
    for me_agent_random in me_agents_random:
        me_agent_random.save()
    for edge_agent_me_random in edge_agents_me_random:
        edge_agent_me_random.save()

    for one_agent in one_agents:
        one_agent.save()

    one_agent_edge[0].save()

    for me_agent_fl in me_agents_fl:
        me_agent_fl.save()
    for edge_agent_fl in edge_agents_fl:
        edge_agent_fl.save()
    for me_agent_fl_td in me_agents_fl_td:
        me_agent_fl_td.save()
    for edge_agent_fl_td in edge_agents_fl_td:
        edge_agent_fl_td.save()

    for me_agent_edge_local in me_agents_edge_local:
        me_agent_edge_local.save()
    for edge_agent_local in edge_agents_local:
        edge_agent_local.save()
    for me_agent_edge_cloud in me_agents_edge_cloud:
        me_agent_edge_cloud.save()
    for edge_agent_cloud in edge_agents_cloud:
        edge_agent_cloud.save()
    for me_agent_edge_random in me_agents_edge_random:
        me_agent_edge_random.save()
    for edge_agent_random in edge_agents_random:
        edge_agent_random.save()

    episode_file = './data/episode.csv'
    if os.path.exists(episode_file):
        os.remove(episode_file)
    torch.save(torch.tensor(episode_worst), episode_file)


def print_info():
    edge_setting.print()
    for me_agent in me_agents:
        me_agent.print()


if __name__ == '__main__':
    time_start = time.time()
    difference = [0, 0, 0, 0]
    episode = [0, 0, 0, 0]

    count = 0

    build_agent()
    for _episode in range(MAX_EPISODES):
        print('episode: ' + str(_episode))
        reset()
        for _step in range(MAX_STEPS):
            count += 1
            train(count)

            # me_agents[0].print()
            # edge_agents[0].print()
            # one_agents[0].print()
        store()
        # print_info()
        if _episode >= 30:
            me_baseline_one_episode(difference, episode, _episode)
            edge_baseline_one_episode(difference, episode, _episode)
            fl_one_episode(difference, episode, _episode)
        if _episode >= 100:
            one_agent_one_episode(difference, episode, _episode)

    # continue train 30 episodes
    for _episode in range(30):
        print('episode: ' + str(_episode + MAX_EPISODES))
        reset_()
        for _step in range(MAX_STEPS):
            train_()
        store_()

    save(episode)

    os.system('cd data && tar -cvf data.tar ./')
    print('worst episode: ', episode)
    time_end = time.time()
    hours, rem = divmod(time_end - time_start, 3600)
    minutes, seconds = divmod(rem, 60)
    print("run time: {:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))
