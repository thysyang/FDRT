# encoding: utf-8
from matplotlib.patches import ConnectionPatch

import main
import Config as config

import numpy as np
import torch
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


def show_reward(flag):
    data = []
    if flag == 0:
        for i in range(config.Edge_num):
            if i & 1 == 1:
                continue
            for j in range(config.ME_num):
                data.append(torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_reward.csv').numpy())
        data = np.nanmean(data, axis=0)
    elif flag == 1:
        for i in range(config.Edge_num):
            if i & 1 == 1:
                continue
            data.append(torch.load('./data/data/prime_edge_' + str(i) + '_reward.csv').numpy())
        data = np.nanmean(data, axis=0)
    elif flag == 2:
        for i in range(config.Edge_num):
            if i & 1 == 1:
                continue
            for j in range(config.ME_num):
                data.append(
                    torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_one_agent_reward.csv').numpy())
        data = np.nanmean(data, axis=0)
    elif flag == 3:
        data = torch.load('./data/data/one_agent_edge_reward.csv')

    x = np.arange(0, main.MAX_EPISODES)

    plt.plot(x, data)
    plt.xlabel('episodes')
    plt.ylabel('rewards')
    plt.show()


def show_one_agent_reward_compare(version, num):
    reward = []
    reward_one_agent = []
    reward_one_agent_edge = torch.load('./data/data/one_agent_edge_reward.csv').numpy()

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            reward.append(torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_reward.csv').numpy())
            reward_one_agent.append(
                torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_one_agent_reward.csv').numpy())

    reward = np.nanmean(reward, axis=0)
    reward_one_agent = np.nanmean(reward_one_agent, axis=0)

    reward = reward[:200]
    reward_one_agent = reward_one_agent[:200]
    reward_one_agent_edge = reward_one_agent_edge[:200]

    x = np.arange(0, main.MAX_EPISODES)
    fig, ax = plt.subplots(1, 1, figsize=(9, 6))
    ax.set_xlabel('episode')
    ax.set_ylabel('reward')
    ax.plot(x, reward, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    ax.plot(x, reward_one_agent, color=(1 / 255, 86 / 255, 153 / 255), label='SMDA')
    ax.plot(x, reward_one_agent_edge, color=(250 / 255, 192 / 255, 15 / 255), label='OMSA')
    ax.legend(loc='lower right')

    axins = inset_axes(ax, width="100%", height="100%", loc='lower left',
                       bbox_to_anchor=(0.16, 0.2, 0.7, 0.6),
                       bbox_transform=ax.transAxes)
    axins.plot(x, reward, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    axins.plot(x, reward_one_agent, color=(1 / 255, 86 / 255, 153 / 255), label='SMDA')
    axins.plot(x, reward_one_agent_edge, color=(250 / 255, 192 / 255, 15 / 255), label='OMSA')
    # X轴的显示范围
    xlim0 = 0
    xlim1 = 100
    # Y轴的显示范围
    ylim0 = -3
    ylim1 = 0
    # 调整子坐标系的显示范围
    axins.set_xlim(xlim0, xlim1)
    axins.set_ylim(ylim0, ylim1)

    ylim0 = -13
    ylim1 = 13
    # 原图中画方框
    tx0 = xlim0
    tx1 = xlim1
    ty0 = ylim0
    ty1 = ylim1
    sx = [tx0, tx1, tx1, tx0, tx0]
    sy = [ty0, ty0, ty1, ty1, ty0]
    ax.plot(sx, sy, "black")
    # 画两条线
    xy = (xlim0, ylim0)
    xy2 = (xlim0, ylim1 - 13)
    con = ConnectionPatch(xyA=xy2, xyB=xy, coordsA="data", coordsB="data",
                          axesA=axins, axesB=ax)
    axins.add_artist(con)

    xy = (xlim1, ylim0)
    xy2 = (xlim1, ylim1 - 13)
    con = ConnectionPatch(xyA=xy2, xyB=xy, coordsA="data", coordsB="data",
                          axesA=axins, axesB=ax)
    axins.add_artist(con)
    plt.savefig('./picture/' + version + '/reward-' + version + '-' + num + '-me-one_agent.png')
    plt.show()


def show_baseline_reward_compare(version, num):
    avg_reward = []
    avg_only_local = []
    avg_only_offload = []
    avg_only_random = []

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            avg_reward.append(torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_reward.csv').numpy())

            avg_only_local.append(
                torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_only_local_reward.csv').numpy())
            avg_only_offload.append(
                torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_only_offload_reward.csv').numpy())
            avg_only_random.append(
                torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_only_random_reward.csv').numpy())

    avg_reward = np.nanmean(avg_reward, axis=0)[:200]
    avg_only_local = np.nanmean(avg_only_local, axis=0)
    avg_only_offload = np.nanmean(avg_only_offload, axis=0)
    avg_only_random = np.nanmean(avg_only_random, axis=0)

    x = np.arange(0, main.MAX_EPISODES)
    plt.rcParams['figure.figsize'] = (9, 6)
    plt.xlabel('episode')
    plt.ylabel('reward')
    plt.plot(x, avg_reward, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    plt.plot(x, avg_only_local, color=(69 / 255, 189 / 255, 155 / 255), label='MDL')
    plt.plot(x, avg_only_offload, color=(43 / 255, 85 / 255, 125 / 255), label='MDO')
    plt.plot(x, avg_only_random, color=(253 / 255, 207 / 255, 110 / 255), label='MDR')
    plt.rcParams['font.sans-serif'] = ['Microsoft YaHei']
    plt.legend(loc='lower right')
    plt.savefig('./picture/' + version + '/reward-' + version + '-' + num + '-me-baseline.png')
    plt.show()


def show_edge_baseline_reward_compare(version, num):
    avg_reward = []
    avg_only_local = []
    avg_only_cloud = []
    avg_only_random = []

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            avg_reward.append(torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_reward.csv').numpy())

            avg_only_local.append(
                torch.load('./data/data/edge_local_edge_' + str(i) + '_me_' + str(j) + '_reward.csv').numpy())
            avg_only_cloud.append(
                torch.load('./data/data/edge_cloud_edge_' + str(i) + '_me_' + str(j) + '_reward.csv').numpy())
            avg_only_random.append(
                torch.load('./data/data/edge_random_edge_' + str(i) + '_me_' + str(j) + '_reward.csv').numpy())

    avg_reward = np.nanmean(avg_reward, axis=0)[:200]
    avg_only_local = np.nanmean(avg_only_local, axis=0)
    avg_only_cloud = np.nanmean(avg_only_cloud, axis=0)
    avg_only_random = np.nanmean(avg_only_random, axis=0)

    x = np.arange(0, main.MAX_EPISODES)
    plt.rcParams['figure.figsize'] = (9, 6)
    plt.xlabel('episode')
    plt.ylabel('reward')
    plt.plot(x, avg_reward, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    plt.plot(x, avg_only_local, color=(95 / 255, 198 / 255, 201 / 255), label='MSL')
    plt.plot(x, avg_only_cloud, color=(243 / 255, 118 / 255, 74 / 255), label='MSC')
    plt.plot(x, avg_only_random, color=(79 / 255, 89 / 255, 100 / 255), label='MSR')
    plt.rcParams['font.sans-serif'] = ['Microsoft YaHei']
    plt.legend(loc='lower right')
    plt.savefig('./picture/' + version + '/reward-' + version + '-' + num + '-edge-baseline.png')
    plt.show()


def show_me_local():
    local = []

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            local.append(torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_local.csv').numpy())

    local = np.nanmean(local, axis=0) / main.MAX_STEPS

    x = np.arange(0, main.MAX_EPISODES)
    plt.plot(x, local)
    plt.show()


def show_edge_action():
    local = []
    cloud = []
    other = []

    for i in range(config.Edge_num):
        local.append(torch.load('./data/data/prime_edge_' + str(i) + '_local.csv').numpy())
        cloud.append(torch.load('./data/data/prime_edge_' + str(i) + '_cloud.csv').numpy())
        other.append(torch.load('./data/data/prime_edge_' + str(i) + '_other.csv').numpy())

    local = np.nanmean(local, axis=0)[:200]
    cloud = np.nanmean(cloud, axis=0)[:200]
    other = np.nanmean(other, axis=0)[:200]

    all_action = np.nansum([local, cloud, other], axis=0)
    local /= all_action
    cloud /= all_action
    other /= all_action

    x = np.arange(0, main.MAX_EPISODES)
    plt.plot(x, local, color='red')
    plt.plot(x, cloud, color='blue')
    plt.plot(x, other, color='green')
    plt.show()


def show_one_agent_action():
    local = []
    edge = []
    cloud = []

    for i in range(config.Edge_num):

        if i & 1 == 1:
            continue

        for j in range(config.ME_num):
            local.append(torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_one_agent_local.csv').numpy())
            edge.append(torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_one_agent_edge.csv').numpy())
            cloud.append(torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_one_agent_cloud.csv').numpy())

    local = np.nanmean(local, axis=0)
    edge = np.nanmean(edge, axis=0)
    cloud = np.nanmean(cloud, axis=0)

    all_action = np.nansum([local, edge, cloud], axis=0)
    local /= all_action
    edge /= all_action
    cloud /= all_action

    x = np.arange(0, main.MAX_EPISODES)
    plt.plot(x, local, color='red')
    plt.plot(x, edge, color='green')
    plt.plot(x, cloud, color='blue')
    plt.show()


def show_one_agent_edge_action():
    local = torch.load('./data/data/one_agent_edge_local.csv')
    edge = torch.load('./data/data/one_agent_edge_edge.csv')
    cloud = torch.load('./data/data/one_agent_edge_cloud.csv')
    all_action = local + edge + cloud
    local = local / all_action
    edge = edge / all_action
    cloud = cloud / all_action

    x = np.arange(0, main.MAX_EPISODES)
    plt.plot(x, local, color='red')
    plt.plot(x, edge, color='green')
    plt.plot(x, cloud, color='blue')
    plt.show()


# 应该是每个episode local、offload的平均时间
def show_me_time():
    local_times = []
    offload_times = []

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            local_times.append(torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_local_times.csv').numpy())
            offload_times.append(
                torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_offload_times.csv').numpy())

    local_times = np.nanmean(local_times, axis=0)
    offload_times = np.nanmean(offload_times, axis=0)

    x = np.arange(0, main.MAX_EPISODES)
    plt.plot(x, local_times, color='red')
    plt.plot(x, offload_times, color='green')
    plt.show()


def show_edge_time():
    local_times = []
    cloud_times = []
    other_times = []

    for i in range(config.Edge_num):
        local_times.append(torch.load('./data/data/prime_edge_' + str(i) + '_local_times.csv').numpy())
        cloud_times.append(torch.load('./data/data/prime_edge_' + str(i) + '_cloud_times.csv').numpy())
        other_times.append(torch.load('./data/data/prime_edge_' + str(i) + '_other_times.csv').numpy())

    local_times = np.nanmean(local_times, axis=0)
    cloud_times = np.nanmean(cloud_times, axis=0)
    other_times = np.nanmean(other_times, axis=0)

    x = np.arange(0, main.MAX_EPISODES)
    plt.plot(x, local_times, color='red')
    plt.plot(x, cloud_times, color='blue')
    plt.plot(x, other_times, color='green')
    plt.show()


def show_time_me_baseline(version, num):
    avg_time = []
    avg_only_local = []
    avg_only_offload = []
    avg_only_random = []

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            avg_time.append(torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_times.csv').numpy())

            avg_only_local.append(
                torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_only_local_times.csv').numpy())
            avg_only_offload.append(
                torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_only_offload_times.csv').numpy())
            avg_only_random.append(
                torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_only_random_times.csv').numpy())

    avg_time = np.nanmean(avg_time, axis=0)[:200]
    avg_only_local = np.nanmean(avg_only_local, axis=0)
    avg_only_offload = np.nanmean(avg_only_offload, axis=0)
    avg_only_random = np.nanmean(avg_only_random, axis=0)

    convergence = 50
    print('my', np.nanmean(avg_time[convergence:]))
    print('only local', np.nanmean(avg_only_local[convergence:]))
    print('only offload', np.nanmean(avg_only_offload[convergence:]))
    print('random', np.nanmean(avg_only_random[convergence:]))

    print('相比于only local提升',
          (np.nanmean(avg_only_local[convergence:]) - np.nanmean(avg_time[convergence:])) /
          np.nanmean(avg_only_local[convergence:]))
    print('相比于only offload提升',
          (np.nanmean(avg_only_offload[convergence:]) - np.nanmean(avg_time[convergence:])) /
          np.nanmean(avg_only_offload[convergence:]))
    print('相比于only random提升',
          (np.nanmean(avg_only_random[convergence:]) - np.nanmean(avg_time[convergence:])) /
          np.nanmean(avg_only_random[convergence:]))

    # avg_time = -avg_time * 200
    # avg_only_local = -avg_only_local * 200
    # avg_only_offload = -avg_only_offload * 200
    # avg_only_random = -avg_only_random * 200

    x = np.arange(0, main.MAX_EPISODES)
    plt.rcParams['figure.figsize'] = (9, 6)
    plt.xlabel('episode')
    plt.ylabel('delay(ms)')
    # plt.plot(x, avg_time, label='本方法', marker='^', color='k')
    # plt.plot(x, avg_only_local, label='仅本地计算', marker='s', color='k')
    # plt.plot(x, avg_only_offload, label='近计算卸载', marker='o', color='k')
    # plt.plot(x, avg_only_random, label='随机卸载', marker='D', color='k')
    plt.plot(x, avg_time, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    plt.plot(x, avg_only_local, color=(69 / 255, 189 / 255, 155 / 255), label='MDL')
    plt.plot(x, avg_only_offload, color=(43 / 255, 85 / 255, 125 / 255), label='MDO')
    plt.plot(x, avg_only_random, color=(253 / 255, 207 / 255, 110 / 255), label='MDR')
    plt.rcParams['font.sans-serif'] = ['Microsoft YaHei']
    plt.legend(loc='upper right')
    plt.savefig('./picture/' + version + '/time-' + version + '-' + num + '-me-baseline.png')
    plt.show()


def show_time_baseline_one_episode(version, num):
    avg_time = []
    avg_only_local = []
    avg_only_offload = []
    avg_only_random = []

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            avg_time.append(
                torch.load(
                    './data/data/edge_' + str(i) + '_me_' + str(j) + '_2_me_baseline_one_episode_times.csv').numpy())

            avg_only_local.append(
                torch.load(
                    './data/data/edge_' + str(i) + '_me_' + str(j) + '_one_episode_only_local_times.csv').numpy())
            avg_only_offload.append(
                torch.load(
                    './data/data/edge_' + str(i) + '_me_' + str(j) + '_one_episode_only_offload_times.csv').numpy())
            avg_only_random.append(
                torch.load(
                    './data/data/edge_' + str(i) + '_me_' + str(j) + '_one_episode_only_random_times.csv').numpy())

    avg_time = np.nanmean(avg_time, axis=0)
    avg_only_local = np.nanmean(avg_only_local, axis=0)
    avg_only_offload = np.nanmean(avg_only_offload, axis=0)
    avg_only_random = np.nanmean(avg_only_random, axis=0)

    print('my', np.nanmean(avg_time))
    print('only local', np.nanmean(avg_only_local))
    print('only offload', np.nanmean(avg_only_offload))
    print('random', np.nanmean(avg_only_random))

    print('相比于only local提升', (np.nanmean(avg_only_local) - np.nanmean(avg_time)) / np.nanmean(avg_only_local))
    print('相比于only offload提升',
          (np.nanmean(avg_only_offload) - np.nanmean(avg_time)) / np.nanmean(avg_only_offload))
    print('相比于only cloud提升', (np.nanmean(avg_only_random) - np.nanmean(avg_time)) / np.nanmean(avg_only_random))

    x = np.arange(0, main.MAX_STEPS)
    plt.rcParams['figure.figsize'] = (9, 6)
    plt.xlabel('step')
    plt.ylabel('delay(ms)')
    plt.plot(x, avg_time, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    # plt.plot(x, avg_only_local, color=(69/255, 189/255, 155/255), label='MDL')
    plt.plot(x, avg_only_offload, color=(43 / 255, 85 / 255, 125 / 255), label='MDO')
    # plt.plot(x, avg_only_random, color=(253/255, 207/255, 110/255), label='MDR')
    plt.rcParams['font.sans-serif'] = ['Microsoft YaHei']
    plt.legend(loc='upper right')
    plt.savefig('./picture/' + version + '/time-' + version + '-' + num + '-baseline-one_episode.png')
    plt.show()


def show_time_baseline_one_episode_worst_me(version, num):
    # 0, 4
    diff = 0
    index_i = 0
    index_j = 0

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            me = torch.load(
                './data/data/edge_' + str(i) + '_me_' + str(j) + '_2_me_baseline_one_episode_times.csv').numpy()
            offload = torch.load(
                './data/data/edge_' + str(i) + '_me_' + str(j) + '_one_episode_only_offload_times.csv').numpy()
            if np.nanmean(offload) - np.nanmean(me) > diff:
                diff = np.nanmean(offload) - np.nanmean(me)
                index_i = i
                index_j = j

    avg_time = torch.load(
        './data/data/edge_' + str(index_i) + '_me_' + str(index_j) + '_2_me_baseline_one_episode_times.csv').numpy()
    avg_only_local = torch.load(
        './data/data/edge_' + str(index_i) + '_me_' + str(index_j) + '_one_episode_only_local_times.csv').numpy()
    avg_only_offload = torch.load(
        './data/data/edge_' + str(index_i) + '_me_' + str(index_j) + '_one_episode_only_offload_times.csv').numpy()

    avg_only_random = torch.load(
        './data/data/edge_' + str(index_i) + '_me_' + str(index_j) + '_one_episode_only_random_times.csv').numpy()

    print('my', np.nanmean(avg_time))
    print('only local', np.nanmean(avg_only_local))
    print('only offload', np.nanmean(avg_only_offload))
    print('random', np.nanmean(avg_only_random))

    print('相比于only local提升', (np.nanmean(avg_only_local) - np.nanmean(avg_time)) / np.nanmean(avg_only_local))
    print('相比于only offload提升',
          (np.nanmean(avg_only_offload) - np.nanmean(avg_time)) / np.nanmean(avg_only_offload))
    print('相比于only cloud提升', (np.nanmean(avg_only_random) - np.nanmean(avg_time)) / np.nanmean(avg_only_random))

    x = np.arange(0, main.MAX_STEPS)
    plt.rcParams['figure.figsize'] = (9, 6)
    plt.xlabel('step')
    plt.ylabel('delay(ms)')
    plt.plot(x, avg_time, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    # plt.plot(x, avg_only_local, color=(69/255, 189/255, 155/255), label='MDL')
    plt.plot(x, avg_only_offload, color=(43 / 255, 85 / 255, 125 / 255), label='MDO')
    # plt.plot(x, avg_only_random, color=(253/255, 207/255, 110/255), label='MDR')
    plt.legend(loc='upper right')
    plt.savefig('./picture/' + version + '/time-' + version + '-' + num + '-baseline-one_episode-worst.png')
    plt.show()


def show_time_baseline_one_episode_(version, num):
    diff = 0
    index_i = 0
    index_j = 0

    time = []
    only_offload = []

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            time.append(
                torch.load(
                    './data/data/edge_' + str(i) + '_me_' + str(j) + '_2_me_baseline_one_episode_times.csv').numpy())
            only_offload.append(
                torch.load(
                    './data/data/edge_' + str(i) + '_me_' + str(j) + '_one_episode_only_offload_times.csv').numpy())

            me = torch.load(
                './data/data/edge_' + str(i) + '_me_' + str(j) + '_2_me_baseline_one_episode_times.csv').numpy()
            offload = torch.load(
                './data/data/edge_' + str(i) + '_me_' + str(j) + '_one_episode_only_offload_times.csv').numpy()
            if np.nanmean(offload) - np.nanmean(me) > diff:
                diff = np.nanmean(offload) - np.nanmean(me)
                index_i = i
                index_j = j

    time = np.nanmean(time, axis=0)
    only_offload = np.nanmean(only_offload, axis=0)

    avg_time = torch.load(
        './data/data/edge_' + str(index_i) + '_me_' + str(index_j) + '_2_me_baseline_one_episode_times.csv').numpy()
    avg_only_offload = torch.load(
        './data/data/edge_' + str(index_i) + '_me_' + str(index_j) + '_one_episode_only_offload_times.csv').numpy()

    x = np.arange(0, main.MAX_STEPS)
    fig, axes = plt.subplots(1, 2, figsize=(18, 6))
    ax1 = axes[0]
    ax1.set_xlabel('step')
    ax1.set_ylabel('delay(ms)')
    ax1.plot(x, time, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    ax1.plot(x, only_offload, color=(43 / 255, 85 / 255, 125 / 255), label='MDO')
    ax1.legend(loc='upper right')
    ax2 = axes[1]
    ax2.set_xlabel('step')
    ax2.set_ylabel('delay(ms)')
    ax2.plot(x, avg_time, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    ax2.plot(x, avg_only_offload, color=(43 / 255, 85 / 255, 125 / 255), label='MDO')
    ax2.legend(loc='upper right')
    plt.savefig('./picture/' + version + '/time-' + version + '-' + num + '-baseline-one_episode_.png')
    plt.show()


def show_time_edge_baseline(version, num):
    avg_time = []
    avg_only_local = []
    avg_only_cloud = []
    avg_only_random = []

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            avg_time.append(torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_times.csv').numpy())

            avg_only_local.append(
                torch.load('./data/data/edge_local_edge_' + str(i) + '_me_' + str(j) + '_times.csv').numpy())
            avg_only_cloud.append(
                torch.load('./data/data/edge_cloud_edge_' + str(i) + '_me_' + str(j) + '_times.csv').numpy())
            avg_only_random.append(
                torch.load('./data/data/edge_random_edge_' + str(i) + '_me_' + str(j) + '_times.csv').numpy())

    avg_time = np.nanmean(avg_time, axis=0)[:200]
    avg_only_local = np.nanmean(avg_only_local, axis=0)
    avg_only_cloud = np.nanmean(avg_only_cloud, axis=0)
    avg_only_random = np.nanmean(avg_only_random, axis=0)

    convergence = 50
    print('my', np.nanmean(avg_time[convergence:]))
    print('only local', np.nanmean(avg_only_local[convergence:]))
    print('only cloud', np.nanmean(avg_only_cloud[convergence:]))
    print('random', np.nanmean(avg_only_random[convergence:]))

    print('相比于only local提升',
          (np.nanmean(avg_only_local[convergence:]) - np.nanmean(avg_time[convergence:])) /
          np.nanmean(avg_only_local[convergence:]))
    print('相比于only cloud提升',
          (np.nanmean(avg_only_cloud[convergence:]) - np.nanmean(avg_time[convergence:])) /
          np.nanmean(avg_only_cloud[convergence:]))
    print('相比于only random提升',
          (np.nanmean(avg_only_random[convergence:]) - np.nanmean(avg_time[convergence:])) /
          np.nanmean(avg_only_random[convergence:]))

    # avg_time = -avg_time * 200
    # avg_only_local = -avg_only_local * 200
    # avg_only_cloud = -avg_only_cloud * 200
    # avg_only_random = -avg_only_random * 200

    x = np.arange(0, main.MAX_EPISODES)
    plt.rcParams['figure.figsize'] = (9, 6)
    plt.xlabel('episode')
    plt.ylabel('delay(ms)')
    plt.plot(x, avg_time, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    plt.plot(x, avg_only_local, color=(95 / 255, 198 / 255, 201 / 255), label='MSL')
    plt.plot(x, avg_only_cloud, color=(243 / 255, 118 / 255, 74 / 255), label='MSC')
    plt.plot(x, avg_only_random, color=(79 / 255, 89 / 255, 100 / 255), label='MSR')
    plt.rcParams['font.sans-serif'] = ['Microsoft YaHei']
    plt.legend(loc='upper right')
    plt.savefig('./picture/' + version + '/time-' + version + '-' + num + '-edge-baseline.png')
    plt.show()


def show_time_edge_baseline_one_episode(version, num):
    avg_time = []
    avg_only_local = []
    avg_only_cloud = []
    avg_only_random = []

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            avg_time.append(
                torch.load(
                    './data/data/edge_' + str(i) + '_me_' + str(j) + '_2_edge_baseline_one_episode_times.csv').numpy())

            avg_only_local.append(
                torch.load(
                    './data/data/edge_local_edge_' + str(i) + '_me_' + str(j) + '_one_episode_times.csv').numpy())
            avg_only_cloud.append(
                torch.load(
                    './data/data/edge_cloud_edge_' + str(i) + '_me_' + str(j) + '_one_episode_times.csv').numpy())
            avg_only_random.append(
                torch.load(
                    './data/data/edge_random_edge_' + str(i) + '_me_' + str(j) + '_one_episode_times.csv').numpy())

    avg_time = np.nanmean(avg_time, axis=0)
    avg_only_local = np.nanmean(avg_only_local, axis=0)
    avg_only_cloud = np.nanmean(avg_only_cloud, axis=0)
    avg_only_random = np.nanmean(avg_only_random, axis=0)

    print('my', np.nanmean(avg_time))
    print('only local', np.nanmean(avg_only_local))
    print('only cloud', np.nanmean(avg_only_cloud))
    print('random', np.nanmean(avg_only_random))

    print('相比于only local提升', (np.nanmean(avg_only_local) - np.nanmean(avg_time)) / np.nanmean(avg_only_local))
    print('相比于only cloud提升',
          (np.nanmean(avg_only_cloud) - np.nanmean(avg_time)) / np.nanmean(avg_only_cloud))
    print('相比于only random提升', (np.nanmean(avg_only_random) - np.nanmean(avg_time)) / np.nanmean(avg_only_random))

    x = np.arange(0, main.MAX_STEPS)
    plt.rcParams['figure.figsize'] = (9, 6)
    plt.xlabel('step')
    plt.ylabel('time(ms)')
    plt.plot(x, avg_time, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    plt.plot(x, avg_only_local, color=(95 / 255, 198 / 255, 201 / 255), label='MSL')
    # plt.plot(x, avg_only_cloud, color=(243 / 255, 118 / 255, 74 / 255), label='MSC')
    # plt.plot(x, avg_only_random, color=(79 / 255, 89 / 255, 100 / 255), label='MSR')
    plt.rcParams['font.sans-serif'] = ['Microsoft YaHei']
    plt.legend(loc='upper right')
    plt.savefig('./picture/' + version + '/time-' + version + '-' + num + '-edge_baseline-one_episode.png')
    plt.show()


def show_time_edge_baseline_one_episode_worst_me():
    diff = 0
    index_i = 0
    index_j = 0

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            me = torch.load(
                './data/data/edge_' + str(i) + '_me_' + str(j) + '_2_edge_baseline_one_episode_times.csv').numpy()
            local = torch.load(
                './data/data/edge_local_edge_' + str(i) + '_me_' + str(
                    j) + '_one_episode_times.csv').numpy()
            if np.nanmean(local) - np.nanmean(me) > diff:
                diff = np.nanmean(local) - np.nanmean(me)
                index_i = i
                index_j = j

    time = torch.load(
        './data/data/edge_' + str(index_i) + '_me_' + str(index_j) + '_2_edge_baseline_one_episode_times.csv').numpy()
    only_local = torch.load(
        './data/data/edge_local_edge_' + str(index_i) + '_me_' + str(index_j) + '_one_episode_times.csv').numpy()
    only_cloud = torch.load(
        './data/data/edge_cloud_edge_' + str(index_i) + '_me_' + str(index_j) + '_one_episode_times.csv').numpy()

    only_random = torch.load(
        './data/data/edge_random_edge_' + str(index_i) + '_me_' + str(index_j) + '_one_episode_times.csv').numpy()

    print('my', np.nanmean(time))
    print('only local', np.nanmean(only_local))
    print('only cloud', np.nanmean(only_cloud))
    print('random', np.nanmean(only_random))

    print('相比于only local提升', (np.nanmean(only_local) - np.nanmean(time)) / np.nanmean(only_local))
    print('相比于only cloud提升',
          (np.nanmean(only_cloud) - np.nanmean(time)) / np.nanmean(only_cloud))
    print('相比于random提升', (np.nanmean(only_random) - np.nanmean(time)) / np.nanmean(only_random))

    x = np.arange(0, main.MAX_STEPS)
    plt.rcParams['figure.figsize'] = (9, 6)
    plt.xlabel('step')
    plt.ylabel('time(ms)')
    plt.plot(x, time, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    plt.plot(x, only_local, color=(95 / 255, 198 / 255, 201 / 255), label='MSL')
    plt.show()


def show_time_one_agent(version, num):
    avg_time = []
    avg_one_agent = []

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            avg_time.append(torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_times.csv').numpy())

            avg_one_agent.append(
                torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_one_agent_times.csv').numpy())

    avg_time = np.nanmean(avg_time, axis=0)[:200]
    avg_one_agent = np.nanmean(avg_one_agent, axis=0)

    one_agent_edge = torch.load('./data/data/one_agent_edge_times.csv').numpy()

    convergence = 25
    print('my ', np.nanmean(avg_time[convergence:]))
    print('one agent ', np.nanmean(avg_one_agent[convergence:]))
    print('one agent edge ', np.nanmean(one_agent_edge[convergence:]))

    print('相比于单智能体部署到mobile提升',
          (np.nanmean(avg_one_agent[10:]) - np.nanmean(avg_time[convergence:])) /
          np.nanmean(avg_one_agent[convergence:]))

    print('相比于单智能体部署到edge提升',
          (np.nanmean(one_agent_edge[convergence:]) - np.nanmean(avg_time[convergence:])) /
          np.nanmean(one_agent_edge[convergence:]))

    # avg_time = -200 * avg_time
    # avg_one_agent = -200 * avg_one_agent
    # one_agent_edge = -200 * one_agent_edge

    x = np.arange(0, main.MAX_EPISODES)
    fig, ax = plt.subplots(1, 1, figsize=(9, 6))
    ax.set_xlabel('episode')
    ax.set_ylabel('delay(ms)')
    # plt.rcParams['font.sans-serif'] = ['SimHei']
    # ax.plot(x, avg_time, label='本方法', marker='s', color='k')
    # ax.plot(x, avg_one_agent, label='端层多智能体方法', marker='<', color='k')
    # ax.plot(x, one_agent_edge, label='边层多智能体方法', marker='d', color='k')
    ax.plot(x, avg_time, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    ax.plot(x, avg_one_agent, color=(1 / 255, 86 / 255, 153 / 255), label='SMDA')
    ax.plot(x, one_agent_edge, color=(250 / 255, 192 / 255, 15 / 255), label='OMSA')
    ax.legend(loc='upper right')

    axins = inset_axes(ax, width="100%", height="100%", loc='lower left',
                       bbox_to_anchor=(0.2, 0.2, 0.7, 0.6),
                       bbox_transform=ax.transAxes)
    # axins.plot(x, avg_time, label='本方法', marker='s', color='k')
    # axins.plot(x, avg_one_agent, label='端层多智能体方法', marker='<', color='k')
    # axins.plot(x, one_agent_edge, label='边层多智能体方法', marker='d', color='k')
    axins.plot(x, avg_time, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    axins.plot(x, avg_one_agent, color=(1 / 255, 86 / 255, 153 / 255), label='SMDA')
    axins.plot(x, one_agent_edge, color=(250 / 255, 192 / 255, 15 / 255), label='OMSA')
    # X轴的显示范围
    xlim0 = 0
    xlim1 = 100
    # Y轴的显示范围
    ylim0 = 0
    ylim1 = 0.02
    # 调整子坐标系的显示范围
    axins.set_xlim(xlim0, xlim1)
    axins.set_ylim(ylim0, ylim1)

    # 原图中画方框
    tx0 = xlim0
    tx1 = xlim1
    ty0 = ylim0
    ty1 = ylim1
    sx = [tx0, tx1, tx1, tx0, tx0]
    sy = [ty0, ty0, ty1, ty1, ty0]
    # ax.plot(sx, sy, "black", linestyle='dashed')
    ax.plot(sx, sy, "black")
    # 画两条线
    xy = (xlim0, ylim1)
    xy2 = (xlim0, ylim0)
    con = ConnectionPatch(xyA=xy2, xyB=xy, coordsA="data", coordsB="data",
                          axesA=axins, axesB=ax)
    axins.add_artist(con)

    xy = (xlim1, ylim1)
    xy2 = (xlim1, ylim0)
    con = ConnectionPatch(xyA=xy2, xyB=xy, coordsA="data", coordsB="data",
                          axesA=axins, axesB=ax)
    axins.add_artist(con)

    plt.savefig('./picture/' + version + '/time-' + version + '-' + num + '-one_agent.png')
    plt.show()


def show_time_one_agent_one_episode(version, num):
    avg_time = []
    avg_one_agent = []
    one_agent_edge = torch.load('./data/data/one_agent_edge_one_episode_times.csv').numpy()
    avg_one_agent_edge = []

    count = 0
    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            avg_time.append(
                torch.load(
                    './data/data/edge_' + str(i) + '_me_' + str(j) + '_2_one_agent_one_episode_times.csv').numpy())

            avg_one_agent.append(
                torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_one_agent_one_episode_times.csv').numpy())

            avg_one_agent_edge.append(one_agent_edge[count * 200: (count + 1) * 200])
            count += 1

    avg_time = np.nanmean(avg_time, axis=0)
    avg_one_agent = np.nanmean(avg_one_agent, axis=0)
    avg_one_agent_edge = np.nanmean(avg_one_agent_edge, axis=0)

    print('my ', np.nanmean(avg_time))
    print('one agent ', np.nanmean(avg_one_agent))
    print('one agent edge ', np.nanmean(avg_one_agent_edge))

    print('相比于单智能体部署到mobile提升',
          (np.nanmean(avg_one_agent) - np.nanmean(avg_time)) / np.nanmean(avg_one_agent))

    print('相比于单智能体部署到edge提升',
          (np.nanmean(one_agent_edge) - np.nanmean(avg_time)) / np.nanmean(one_agent_edge))

    x = np.arange(0, main.MAX_STEPS)
    plt.xlabel('episode')
    plt.ylabel('delay(ms)')
    plt.plot(x, avg_time, color='red', label='DRT')
    plt.plot(x, avg_one_agent, color='green', label='SMDA')
    plt.plot(x, avg_one_agent_edge, color='blue', label='OMSA')
    plt.savefig('./picture/' + version + '/time-' + version + '-' + num + '-one_agent-one_episode.png')
    plt.show()


def show_time_one_agent_one_episode_worst_me():
    diff = 0
    index_i = 0
    index_j = 0
    one_agent_edge = torch.load('./data/data/one_agent_edge_one_episode_times.csv').numpy()

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            me = torch.load(
                './data/data/edge_' + str(i) + '_me_' + str(j) + '_2_one_agent_one_episode_times.csv').numpy()
            one_agent = torch.load(
                './data/data/edge_' + str(i) + '_me_' + str(j) + '_one_agent_one_episode_times.csv').numpy()
            if np.nanmean(one_agent) - np.nanmean(me) > diff:
                diff = np.nanmean(one_agent) - np.nanmean(me)
                index_i = i
                index_j = j

    count = int(index_i / 2 * config.ME_num + index_j)

    time = torch.load(
        './data/data/edge_' + str(index_i) + '_me_' + str(index_j) + '_2_one_agent_one_episode_times.csv').numpy()
    one_agent_time = torch.load(
        './data/data/edge_' + str(index_i) + '_me_' + str(index_j) + '_one_agent_one_episode_times.csv').numpy()
    one_agent_edge_time = one_agent_edge[count * 200: (count + 1) * 200]

    print('my ', np.nanmean(time))
    print('one agent ', np.nanmean(one_agent_time))
    print('one agent edge ', np.nanmean(one_agent_edge_time))

    print('相比于单智能体部署到mobile提升',
          (np.nanmean(one_agent_time) - np.nanmean(time)) / np.nanmean(one_agent_time))
    print('相比于单智能体部署到mobile提升',
          (np.nanmean(one_agent_edge_time) - np.nanmean(time)) / np.nanmean(one_agent_edge_time))

    x = np.arange(0, main.MAX_STEPS)
    plt.plot(x, time, color='red')
    plt.plot(x, one_agent_time, color='green')
    plt.plot(x, one_agent_edge_time, color='blue')
    plt.show()


def show_time_fl(version, num):
    time = []
    time_fl = []
    time_fl_td = []

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            time.append(torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_times.csv').numpy())

            time_fl.append(torch.load('./data/data/fl_edge_' + str(i) + '_me_' + str(j) + '_times.csv').numpy())

            time_fl_td.append(torch.load('./data/data/fl_td_edge_' + str(i) + '_me_' + str(j) + '_times.csv').numpy())

    time = np.nanmean(time, axis=0)
    time_fl = np.nanmean(time_fl, axis=0)
    time_fl_td = np.nanmean(time_fl_td, axis=0)

    print('my ', np.nanmean(time[200:]))
    print('fl ', np.nanmean(time_fl[200:]))
    print('td fl ', np.nanmean(time_fl_td[200:]))

    print('相比于非fl提升',
          (np.nanmean(time[200:]) - np.nanmean(time_fl[200:])) / np.nanmean(time[200:]))

    print('相比于fl td提升',
          (np.nanmean(time_fl_td[200:]) - np.nanmean(time_fl[200:])) / np.nanmean(time_fl_td[200:]))

    time = time[:200]
    time_fl = time_fl[:200]
    time_fl_td = time_fl_td[:200]

    x = np.arange(0, main.MAX_EPISODES)
    fig, ax = plt.subplots(1, 1, figsize=(9, 6))
    ax.set_xlabel('episode')
    ax.set_ylabel('delay(ms)')

    line1, = ax.plot(x, time, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    line2, = ax.plot(x, time_fl_td, color=(89 / 255, 169 / 255, 90 / 255), label='TFL')
    line3, = ax.plot(x, time_fl, color=(56 / 255, 89 / 255, 137 / 255), label='FDRT')

    ax.legend(handles=[line3, line1, line2], labels=['FDRT', 'DRT', 'TFL'], loc='upper right')

    axins = inset_axes(ax, width="100%", height="100%", loc='lower left',
                       bbox_to_anchor=(0.3, 0.42, 0.5, 0.4),
                       bbox_transform=ax.transAxes)
    axins.plot(x, time, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    axins.plot(x, time_fl_td, color=(89 / 255, 169 / 255, 90 / 255), label='TFL')
    axins.plot(x, time_fl, color=(56 / 255, 89 / 255, 137 / 255), label='FDRT')
    # X轴的显示范围
    xlim0 = 75
    xlim1 = 150
    # Y轴的显示范围
    ylim0 = 0.0042
    ylim1 = 0.0056
    # 调整子坐标系的显示范围
    axins.set_xlim(xlim0, xlim1)
    axins.set_ylim(ylim0, ylim1)

    # 原图中画方框
    tx0 = xlim0
    tx1 = xlim1
    ty0 = ylim0
    ty1 = ylim1
    sx = [tx0, tx1, tx1, tx0, tx0]
    sy = [ty0, ty0, ty1, ty1, ty0]
    ax.plot(sx, sy, "black")
    # 画两条线
    xy = (xlim0, ylim1)
    xy2 = (xlim0, ylim0)
    con = ConnectionPatch(xyA=xy2, xyB=xy, coordsA="data", coordsB="data",
                          axesA=axins, axesB=ax)
    axins.add_artist(con)

    xy = (xlim1, ylim1)
    xy2 = (xlim1, ylim0)
    con = ConnectionPatch(xyA=xy2, xyB=xy, coordsA="data", coordsB="data",
                          axesA=axins, axesB=ax)
    axins.add_artist(con)
    plt.savefig('./picture/' + version + '/time-' + version + '-' + num + '-fl.png')
    plt.show()


def show_time_fl_continue(version, num):
    time = []
    time_fl = []
    time_fl_td = []

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            time.append(torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_times.csv').numpy())

            time_fl.append(torch.load('./data/data/fl_edge_' + str(i) + '_me_' + str(j) + '_times.csv').numpy())

            time_fl_td.append(torch.load('./data/data/fl_td_edge_' + str(i) + '_me_' + str(j) + '_times.csv').numpy())

    time = np.nanmean(time, axis=0)
    time_fl = np.nanmean(time_fl, axis=0)
    time_fl_td = np.nanmean(time_fl_td, axis=0)

    time = time[200:]
    time_fl = time_fl[200:]
    time_fl_td = time_fl_td[200:]

    print('my ', np.nanmean(time))
    print('fl ', np.nanmean(time_fl))
    print('td fl ', np.nanmean(time_fl_td))

    print('相比于非fl提升',
          (np.nanmean(time) - np.nanmean(time_fl)) / np.nanmean(time))

    print('相比于fl td提升',
          (np.nanmean(time_fl_td) - np.nanmean(time_fl)) / np.nanmean(time_fl_td))

    x = np.arange(200, 230)
    plt.rcParams['figure.figsize'] = (9, 6)
    plt.xlabel('episode')
    plt.ylabel('delay(ms)')
    line1, = plt.plot(x, time, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    line2, = plt.plot(x, time_fl_td, color=(89 / 255, 169 / 255, 90 / 255), label='TFL')
    line3, = plt.plot(x, time_fl, color=(56 / 255, 89 / 255, 137 / 255), label='FDRT')

    plt.legend(handles=[line3, line1, line2], labels=['FDRT', 'DRT', 'TFL'], loc='upper right')
    plt.savefig('./picture/' + version + '/time-' + version + '-' + num + '-fl_continue.png')
    plt.show()


def show_time_fl_one_episode(version, num):
    time = []
    time_fl = []
    time_fl_td = []

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            time.append(
                torch.load(
                    './data/data/edge_' + str(i) + '_me_' + str(j) + '_2_fl_one_episode_times.csv').numpy())

            time_fl.append(
                torch.load('./data/data/fl_edge_' + str(i) + '_me_' + str(j) + '_one_episode_times.csv').numpy())

            time_fl_td.append(
                torch.load('./data/data/fl_td_edge_' + str(i) + '_me_' + str(j) + '_one_episode_times.csv').numpy())

    time = np.nanmean(time, axis=0)
    time_fl = np.nanmean(time_fl, axis=0)
    time_fl_td = np.nanmean(time_fl_td, axis=0)

    print('my ', np.nanmean(time))
    print('one agent ', np.nanmean(time_fl))
    print('one agent edge ', np.nanmean(time_fl_td))

    print('相比于单智能体部署到mobile提升',
          (np.nanmean(time) - np.nanmean(time_fl)) / np.nanmean(time))

    print('相比于单智能体部署到edge提升',
          (np.nanmean(time_fl_td) - np.nanmean(time_fl)) / np.nanmean(time_fl_td))

    x = np.arange(0, main.MAX_STEPS)
    plt.xlabel('episode')
    plt.ylabel('time(ms)')
    plt.plot(x, time, color='red', label='DRT')
    plt.plot(x, time_fl, color='green', label='FDRT')
    plt.plot(x, time_fl_td, color='blue', label='TFL')
    plt.savefig('./picture/' + version + '/time-' + version + '-' + num + '-fl-one_episode.png')
    plt.show()


def show_loss_fl(version, num):
    loss = []
    loss_fl = []
    loss_fl_td = []

    for i in range(config.Edge_num):
        if i & 1 == 1:
            continue
        for j in range(config.ME_num):
            loss.append(torch.load('./data/data/edge_' + str(i) + '_me_' + str(j) + '_loss.csv').numpy())

            loss_fl.append(torch.load('./data/data/fl_edge_' + str(i) + '_me_' + str(j) + '_loss.csv').numpy())

            loss_fl_td.append(torch.load('./data/data/fl_td_edge_' + str(i) + '_me_' + str(j) + '_loss.csv').numpy())

    loss = np.nanmean(loss, axis=0)[:200]
    loss_fl = np.nanmean(loss_fl, axis=0)[:200]
    loss_fl_td = np.nanmean(loss_fl_td, axis=0)[:200]

    x = np.arange(0, main.MAX_EPISODES)
    fig, ax = plt.subplots(1, 1, figsize=(9, 6))
    ax.set_xlabel('episode')
    ax.set_ylabel('loss')

    line1, = ax.plot(x, loss, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    line2, = ax.plot(x, loss_fl_td, color=(89 / 255, 169 / 255, 90 / 255), label='TFL')
    line3, = ax.plot(x, loss_fl, color=(56 / 255, 89 / 255, 137 / 255), label='FDRT')

    ax.legend(handles=[line3, line1, line2], labels=['FDRT', 'DRT', 'TFL'], loc='upper right')

    axins = inset_axes(ax, width="100%", height="100%", loc='lower left',
                       bbox_to_anchor=(0.2, 0.2, 0.7, 0.6),
                       bbox_transform=ax.transAxes)
    axins.plot(x, loss, color=(240 / 255, 81 / 255, 121 / 255), label='DRT')
    axins.plot(x, loss_fl_td, color=(89 / 255, 169 / 255, 90 / 255), label='TFL')
    axins.plot(x, loss_fl, color=(56 / 255, 89 / 255, 137 / 255), label='FDRT')
    # X轴的显示范围
    xlim0 = 0
    xlim1 = 100
    # Y轴的显示范围
    ylim0 = -0.01
    ylim1 = 0.1
    # 调整子坐标系的显示范围
    axins.set_xlim(xlim0, xlim1)
    axins.set_ylim(ylim0, ylim1)

    # 原图中画方框
    tx0 = xlim0
    tx1 = xlim1
    ty0 = ylim0
    ty1 = ylim1
    sx = [tx0, tx1, tx1, tx0, tx0]
    sy = [ty0, ty0, ty1, ty1, ty0]
    ax.plot(sx, sy, "black")
    # 画两条线
    xy = (xlim0, ylim1)
    xy2 = (xlim0, ylim0)
    con = ConnectionPatch(xyA=xy2, xyB=xy, coordsA="data", coordsB="data",
                          axesA=axins, axesB=ax)
    axins.add_artist(con)

    xy = (xlim1, ylim1)
    xy2 = (xlim1, ylim0)
    con = ConnectionPatch(xyA=xy2, xyB=xy, coordsA="data", coordsB="data",
                          axesA=axins, axesB=ax)
    axins.add_artist(con)

    plt.savefig('./picture/' + version + '/loss-' + version + '-' + num + '-fl.png')
    plt.show()


def show(flag, sign, version, num, edge_num, me_num):
    # show_time_baseline_one_episode_(version, num)
    if sign == 0:
        show_me_local()
    elif sign == 1:
        show_edge_action()
    elif sign == 2:
        show_one_agent_action()
    elif sign == 3:
        show_one_agent_edge_action()
    elif sign == 4:
        show_reward(flag)
    elif sign == 5:
        show_baseline_reward_compare(version, num)
    elif sign == 6:
        show_me_time()
    elif sign == 7:
        show_edge_time()
    elif sign == 8:
        show_time_me_baseline(version, num)
    elif sign == 9:
        show_time_edge_baseline(version, num)
    elif sign == 10:
        show_time_one_agent(version, num)
    elif sign == 11:
        show_time_baseline_one_episode(version, num)
    elif sign == 12:
        show_time_baseline_one_episode_worst_me(version, num)
    elif sign == 13:
        show_time_one_agent_one_episode(version, num)
    elif sign == 14:
        show_time_one_agent_one_episode_worst_me()
    elif sign == 15:
        show_time_edge_baseline_one_episode(version, num)
    elif sign == 16:
        show_time_edge_baseline_one_episode_worst_me()
    elif sign == 17:
        show_time_fl(version, num)
    elif sign == 18:
        show_time_fl_one_episode(edge_num, me_num)
    elif sign == 19:
        show_loss_fl(version, num)
    elif sign == 20:
        show_baseline_reward_compare(version, num)
    elif sign == 21:
        show_edge_baseline_reward_compare(version, num)
    elif sign == 22:
        show_one_agent_reward_compare(version, num)
    elif sign == 23:
        show_time_fl_continue(version, num)


def f():
    # 0-me's local action ratio, 1-edge's action ratio, 2-one agent's action ratio, 3-one agent edge's action ratio
    # 4-reward, 5-baseline reward compare
    # 6-me local,offload time, 7-edge local,cloud,other edge time
    # 8-avg task time me baseline, 9-edge baseline, 10-avg task time one agent
    # 11-avg task time baseline one episode, 12-avg task time baseline one episode worst me
    # 13-avg task time one agent one episode, 14-avg task time one agent one episode worst me
    # 15-avg task time edge baseline one episode, 16-avg task time edge baseline one episode worst me
    # 17-avg task time fl, 18-fl me one episode time
    # 19-fl loss
    # 20-baseline reward compare, 21-edge baseline reward compare, 22-one agent reward compare
    # 23-avg task time fl continue
    sign = 22
    flag = 1  # sign=4: 0-me, 1-edge, 2-one agent, 3-one agent edge
    version = '2.7'
    num = '1'
    edge_num = 0
    me_num = 0

    show(flag, sign, version, num, edge_num, me_num)


if __name__ == '__main__':
    f()
