from ME_Agent import ME_Agent
import time
import os

if __name__ == '__main__':
    time_start = time.time()

    me_agent_test = ME_Agent()
    me_agent_test.train(1)

    time_end = time.time()
    print('total time: ', time_end - time_start)
    os.system('cd data && tar -cvf data.tar ./')
